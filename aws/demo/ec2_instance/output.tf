output "account_id" {
  value = data.aws_caller_identity.current.account_id
}

output "ec2_instance_arn" {
  value = resource.aws_instance.web.arn
}