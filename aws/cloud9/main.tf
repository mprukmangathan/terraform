data "aws_caller_identity" "current" {}

resource "aws_vpc" "myvpc" {
  cidr_block = "10.120.0.0/16"

  tags = {
    Name = "MyVpc"
  }
}

resource "aws_internet_gateway" "myigw" {
  vpc_id = aws_vpc.myvpc.id

  tags = {
    Name = "MyIgw"
  }
}

resource "aws_subnet" "private_sub" {
  vpc_id     = aws_vpc.myvpc.id
  cidr_block = "10.120.10.0/24"

  tags = {
    Name = "private-sub"
  }

  depends_on = [aws_internet_gateway.myigw]
}

resource "aws_subnet" "public_sub" {
  vpc_id     = aws_vpc.myvpc.id
  cidr_block = "10.120.20.0/24"

  tags = {
    Name = "public-sub"
  }

  depends_on = [aws_internet_gateway.myigw]
}

resource "aws_eip" "myeip" {
  tags = {
    Name = "MyEip"
  }
}

resource "aws_nat_gateway" "mynatgateway" {
  allocation_id = aws_eip.myeip.id
  subnet_id     = aws_subnet.public_sub.id

  tags = {
    Name = "MyNatGateway"
  }
}

resource "aws_route" "myroute" {
  route_table_id         = aws_vpc.myvpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.mynatgateway.id
}

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.myvpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myigw.id
  }

  tags = {
    Name = "public-rt"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public_sub.id
  route_table_id = aws_route_table.public_rt.id
}

resource "aws_cloud9_environment_ec2" "private_cloud9" {
  instance_type   = "t2.micro"
  name            = "private"
  connection_type = "CONNECT_SSM"
  image_id        = "amazonlinux-2-x86_64"
  subnet_id       = aws_subnet.private_sub.id
  owner_arn       = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"

  depends_on = [
    aws_iam_role_policy_attachment.cloud9
  ]
}

data "aws_iam_policy" "cloud9_ssm_instance_profile" {
  name = "AWSCloud9SSMInstanceProfile"
}

resource "aws_iam_role" "cloud9_access_role" {
  name = "AWSCloud9SSMAccessRole"
  path = "/service-role/"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = [
            "ec2.amazonaws.com",
            "cloud9.amazonaws.com"
          ]
        }
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "cloud9" {
  role       = aws_iam_role.cloud9_access_role.name
  policy_arn = data.aws_iam_policy.cloud9_ssm_instance_profile.arn
}
