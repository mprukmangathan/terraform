
data "aws_kms_key" "backup_kms_key" {
  key_id = "alias/aws/backup"
}

data "aws_kms_key" "backup_kms_key_secondary" {
  key_id   = "alias/aws/backup"
  provider = aws.secondary
}

# AWS Backup vault for storing backups with KMS encryption.
resource "aws_backup_vault" "vault" {
  name        = "DDB-vault"
  kms_key_arn = data.aws_kms_key.backup_kms_key.arn
}
#removing the below code as part of ECP-321

resource "aws_backup_vault" "vault_secondary" {
  name        = "DDB-vault"
  kms_key_arn = data.aws_kms_key.backup_kms_key_secondary.arn
  provider    = aws.secondary
}

# AWS backups plan to Run the backups on specific interval
resource "aws_backup_plan" "DDB_backup_interval" {
  name = "DDB-backup-Interval"
  rule {
    rule_name                = "daily"
    target_vault_name        = aws_backup_vault.vault.name
    schedule                 = "cron(45 7 * * ? *)"
    enable_continuous_backup = false
    lifecycle {
      delete_after = 10
    }
    #removing the below code as part of ECP-321
    copy_action {
      destination_vault_arn = aws_backup_vault.vault_secondary.arn
      lifecycle {
        delete_after = 10
      }
    }
  }
}

resource "aws_iam_role" "example" {
  name               = "example"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": ["sts:AssumeRole"],
      "Effect": "allow",
      "Principal": {
        "Service": ["backup.amazonaws.com"]
      }
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "example" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.example.name
}

# Attaching DDB resources to be backed up as part of backup plan
resource "aws_backup_selection" "daily" {
  name         = "all-DDB-selection"
  iam_role_arn = aws_iam_role.example.arn
  plan_id      = aws_backup_plan.DDB_backup_interval.id
  resources    = ["arn:aws:dynamodb:*:*:table/*"]

  depends_on = [
    aws_backup_region_settings.advance_ddb_settings
  ]
}

resource "aws_backup_region_settings" "advance_ddb_settings" {
  resource_type_opt_in_preference = {
    "Aurora"          = true
    "DocumentDB"      = true
    "DynamoDB"        = true
    "EBS"             = true
    "EC2"             = true
    "EFS"             = true
    "FSx"             = true
    "Neptune"         = true
    "RDS"             = true
    "Storage Gateway" = true
    "VirtualMachine"  = true
  }

  resource_type_management_preference = {
    "DynamoDB" = true
    "EFS"      = true
  }
}

/*
resource "aws_backup_global_settings" "test" {
  global_settings = {
    "isCrossAccountBackupEnabled" = "true"
  }
}
*/