resource "aws_ebs_volume" "volumes" {
  availability_zone = var.availability_zone
  size              = var.size
  type              = var.type

  tags = {
    Name = format("%s-%s", "HelloWorld", var.availability_zone)
  }
}

variable "availability_zone" {
  default = "us-east-2a"
}

variable "size" {
  default = 5
}

variable "type" {
  default = "gp2"
}