
locals {
  ebs_options = [

    {
      az       = "us-east-2a"
      size     = 10
      type     = "gp2"
      vol_name = "useast2a"
    },
    {
      az       = "us-east-2b"
      size     = 10
      type     = "gp2"
      vol_name = "useast2b"
    },
    {
      az       = "us-east-2c"
      size     = 10
      type     = "gp2"
      vol_name = "useast2c"
    }

  ]

  segments = {
    "development" = local.ebs_options
    "preprod"     = local.ebs_options
    "master"      = local.ebs_options
    "temp"        = local.ebs_options
  }

  contexts   = local.segments[data.external.git.result.branch]
  volumes    = { for obj in local.contexts : obj.az => obj }
  is_preprod = data.external.git.result.branch == "development" ? true : false

}


# branch = ${data.external.git.result.branch}
data "external" "git" {
  program = ["sh", "${path.module}/branch.sh"]
}

module "volumes" {
  source            = "./module/"
  for_each          = local.is_preprod ? local.volumes : {}
  availability_zone = lookup(each.value, "az", "")
  size              = lookup(each.value, "size", 10)
  type              = lookup(each.value, "type", "")
}

output "volumes" {
  value = local.volumes
}
