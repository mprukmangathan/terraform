#!/bin/sh

branch_name=$(git rev-parse --abbrev-ref HEAD)

jq -n --arg branch "$branch_name" '{"branch":$branch}'