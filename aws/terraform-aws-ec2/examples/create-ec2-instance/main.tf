provider "aws" {
  region = "us-east-1"
}

data "aws_kms_alias" "ebs" {
  name = "alias/aws/ebs"
}

module "ec2" {
  source                 = "../.."
  instance_count         = 1
  name                   = "test-instance"
  ami                    = "ami-0bfc7e8d42cdeacf9"
  instance_type          = "t2.micro"
  subnet_id              = "subnet-9840c2fe"
  vpc_security_group_ids = ["sg-04671e8b19fff1d71"]
  root_block_device = [
    {
      delete_on_termination = true
      volume_type           = "gp2"
      volume_size           = 10
    },
  ]

  ebs_block_device = [
    {
      device_name = "/dev/sdf"
      volume_type = "gp2"
      volume_size = 5
      encrypted   = true
      kms_key_id  = data.aws_kms_alias.ebs.arn
    }
  ]
  enable_volume_tags = true
  tags = {
    "Env"      = "Private"
    "Location" = "Secret"
  }
  volume_tags = {
    "Name"     = "my-volume"
    "Location" = "Secret"
  }
}
