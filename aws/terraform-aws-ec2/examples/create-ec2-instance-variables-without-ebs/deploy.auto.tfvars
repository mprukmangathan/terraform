region                 = "us-east-1"
instance_count         = 1
name                   = "test-instance"
ami                    = "ami-0bfc7e8d42cdeacf9"
instance_type          = "t2.micro"
subnet_id              = "subnet-9840c2fe"
vpc_security_group_ids = ["sg-04671e8b19fff1d71"]
root_block_device = [
  {
    delete_on_termination = true
    volume_type           = "gp2"
    volume_size           = 10
  }
]
enable_volume_tags = true
tags = {
  "Env"      = "Private"
  "Location" = "Secret"
}
volume_tags = {
  "Name"     = "my-volume"
  "Location" = "Secret"
}
