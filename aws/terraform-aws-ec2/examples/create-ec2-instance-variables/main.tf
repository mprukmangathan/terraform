provider "aws" {
  region = var.region
}

module "ec2_instance" {
  source                 = "git::https://www.bitbucket.org/mprukmangathan/terraform.git//terraform-aws-ec2"
  instance_count         = var.instance_count
  name                   = var.name
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.vpc_security_group_ids
  root_block_device      = var.root_block_device
  ebs_block_device       = var.ebs_block_device
  enable_volume_tags     = var.enable_volume_tags
  tags                   = var.tags
  volume_tags            = var.volume_tags
}
