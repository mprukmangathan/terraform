region                 = "us-east-1"
instance_count         = 1
name                   = "test-instance"
ami                    = "ami-0bfc7e8d42cdeacf9"
instance_type          = "t2.micro"
subnet_id              = "subnet-9840c2fe"
vpc_security_group_ids = ["sg-04671e8b19fff1d71"]
root_block_device = [
  {
    delete_on_termination = true
    volume_type           = "gp2"
    volume_size           = 10
  }
]
ebs_block_device = [
  {
    device_name = "/dev/sdf"
    volume_type = "gp2"
    volume_size = 5
    encrypted   = true
    kms_key_id  = "arn:aws:kms:us-east-1:616589272781:key/c41e88f9-8614-4020-b65a-633340516857"
  }
]
enable_volume_tags = true
tags = {
  "Env"      = "Private"
  "Location" = "Secret"
}
volume_tags = {
  "Name"     = "my-volume"
  "Location" = "Secret"
}
