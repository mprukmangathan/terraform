/**
 * Copyright 2022 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This file was automatically generated from a template in ./autogen/main

/******************************************
  Manage kube-dns configmaps
 *****************************************/

module "kube-dns" {
  source = "./modules/config_map_v1_data"
  count  = local.custom_kube_dns_config && !local.upstream_nameservers_config ? 1 : 0

  metadata = [{
    name      = "kube-dns"
    namespace = "kube-system"
  }]

  data = {
    stubDomains = <<EOF
${jsonencode(var.stub_domains)}
EOF
  }

  depends_on = [
    google_container_cluster.primary,
    google_container_node_pool.pools,
  ]
}

module "kube-dns-upstream-namservers" {
  source = "./modules/config_map_v1_data"
  count  = !local.custom_kube_dns_config && local.upstream_nameservers_config ? 1 : 0

  metadata = [{
    name      = "kube-dns"
    namespace = "kube-system"
  }]

  data = {
    upstreamNameservers = <<EOF
${jsonencode(var.upstream_nameservers)}
EOF
  }

  depends_on = [
    google_container_cluster.primary,
    google_container_node_pool.pools,
  ]
}

module "kube-dns-upstream-nameservers-and-stub-domains" {
  source = "./modules/config_map_v1_data"
  count  = local.custom_kube_dns_config && local.upstream_nameservers_config ? 1 : 0

  metadata = [{
    name      = "kube-dns"
    namespace = "kube-system"
  }]

  data = {
    upstreamNameservers = <<EOF
${jsonencode(var.upstream_nameservers)}
EOF

    stubDomains = <<EOF
${jsonencode(var.stub_domains)}
EOF
  }

  depends_on = [
    google_container_cluster.primary,
    google_container_node_pool.pools,
  ]
}
