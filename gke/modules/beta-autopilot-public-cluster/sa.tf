/**
 * Copyright 2022 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// This file was automatically generated from a template in ./autogen/main

locals {
  service_account_list = compact(
    concat(
      google_service_account.cluster_service_account.*.email,
      ["dummy"],
    ),
  )
  // if user set var.service_account it will be used even if var.create_service_account==true, so service account will be created but not used
  service_account = (var.service_account == "" || var.service_account == "create") && var.create_service_account ? local.service_account_list[0] : var.service_account

  registry_projects_list = length(var.registry_project_ids) == 0 ? [var.project_id] : var.registry_project_ids
}

resource "random_string" "cluster_service_account_suffix" {
  upper   = false
  lower   = true
  special = false
  length  = 4
}

module "cluster_service_account" {
  source = "../cluster-iam/service-account"
  count        = var.create_service_account ? 1 : 0
  project      = var.project_id
  account_id   = "tf-gke-${substr(var.name, 0, min(15, length(var.name)))}-${random_string.cluster_service_account_suffix.result}"
  display_name = "Terraform-managed service account for cluster ${var.name}"
}

module "cluster_service_account_project_iam" {
  source  = "../cluster-iam/project-iam"
  count   = var.create_service_account ? 1 : 0
  project = module.cluster_service_account[0].project
  bindings = {
    "roles/logging.logWriter"                   = module.cluster_service_account[0].member
    "roles/monitoring.metricWriter"             = module.cluster_service_account[0].member
    "roles/monitoring.viewer"                   = module.cluster_service_account[0].member
    "roles/stackdriver.resourceMetadata.writer" = module.cluster_service_account[0].member
  }
}

module "cluster_service_account_registry_project_iam" {
  source   = "../cluster-iam/project-iam"
  for_each = var.create_service_account && var.grant_registry_access ? toset(local.registry_projects_list) : []
  project  = each.key
  bindings = {
    "roles/storage.objectViewer"    = module.cluster_service_account[0].member
    "roles/artifactregistry.reader" = module.cluster_service_account[0].member
  }
}

