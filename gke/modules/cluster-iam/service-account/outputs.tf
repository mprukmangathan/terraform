output "project" {
    value = google_service_account.service_account.project
}

output "id" {
    value = google_service_account.service_account.id
}

output "email" {
    value = google_service_account.service_account.email
}

output "name" {
    value = google_service_account.service_account.name
}

output "member" {
    value = google_service_account.service_account.member
}