variable "project" {
  type        = string
  default     = ""
  description = "The project id for service account."
}

variable "account_id" {
  type        = string
  default     = ""
  description = "The service account id."
}

variable "display_name" {
  type        = string
  default     = ""
  description = "The display name of service account."
}

variable "description" {
  type        = string
  default     = ""
  description = "The description of service account."
}