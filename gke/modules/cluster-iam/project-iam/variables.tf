variable "project" {
  type        = string
  default     = ""
  description = "The project id for IAM binding"
}

variable "bindings" {
  type        = any
  description = <<EOT
    Bindings argument should be in the format:
    {
        "roles/firebase.admin" = ["user:firebase-admin@example.com", "group:firebase-admin-team@example.com"]
        "roles/editor"         = ["group:admin@example.com"]
    }
    EOT
}