variable "metadata" {
  type        = any
  default     = []
  description = "List of standard config map metadata."
}

variable "data" {
  type        = map(any)
  default     = {}
  description = "Data contains the configuration data, The keys stored in Data must not overlap with the keys in the BinaryData field, this is enforced during validation process."
}

variable "binary_data" {
  type        = map(any)
  default     = {}
  description = "BinaryData contains the binary data, This field only accepts base64-encoded payloads that will be decoded/received before being sent/received to the apiserver."
}