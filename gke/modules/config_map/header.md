# GKE IAM Module

This module handles creation of Project IAM for GKE cluster. 
The resources/services/activations/deletions that this module will create/trigger are:
- Set IAM Permissions

## Compatibility

This module is meant for use with Terraform 0.13+ and tested using Terraform 1.0+.
If you find incompatibilities using Terraform `>=0.13`, please open an issue.

If you haven't [upgraded][terraform-0.13-upgrade] and need a Terraform
0.12.x-compatible version of this module, the last released version
intended for Terraform 0.12.x is [12.3.0].

## Usage
simple usage is as follows:

```
module "network" {
  source = "../../../modules/network"

  project_id            = var.project_id
  region                = "us-central1"
  name                  = "gke-test"
  ip_cidr_range         = "10.0.0.0/16"
  pod_ip_cidr_range     = "10.1.0.0/16"
  service_ip_cidr_range = "10.2.0.0/16"
}
```