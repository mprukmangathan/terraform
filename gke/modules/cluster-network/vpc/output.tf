output "network" {
  value = google_compute_network.main
}

output "subnetwork" {
  value = google_compute_subnetwork.main
}

output "network_project" {
  value = google_compute_network.main.project
}

output "name" {
  value = google_compute_network.main.name
}

output "subnet_name" {
  value = google_compute_subnetwork.main.name
}

output "ip_range_pods" {
  value = google_compute_subnetwork.main.secondary_ip_range[0].range_name
}

output "ip_range_services" {
  value = google_compute_subnetwork.main.secondary_ip_range[1].range_name
}