variable "name" {
  type        = string
  default     = ""
  description = "The name for network and subnetwork"
}

variable "project_id" {
  type        = string
  default     = ""
  description = "The project id for network"
}

variable "region" {
  type        = string
  default     = ""
  description = "The region in which the network will be created"
}

variable "ip_cidr_range" {
  type        = string
  default     = ""
  description = "The ip range for subnetwork"
}

variable "pod_ip_cidr_range" {
  type        = string
  default     = ""
  description = "The secondary ip range for GKE pods"
}

variable "service_ip_cidr_range" {
  type        = string
  default     = ""
  description = "The secondary ip range for GKE services"
}