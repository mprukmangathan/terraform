variable "metadata" {
  type        = any
  default     = []
  description = "List of standard config map metadata."
}

variable "data" {
  type        = map(any)
  default     = {}
  description = "Data contains the configuration data, The keys stored in Data must not overlap with the keys in the BinaryData field, this is enforced during validation process."
}

variable "force" {
  type        = bool
  default     = true
  description = "Force management of the configured data if there is a conflict."
}