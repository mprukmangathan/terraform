/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/***********************************
  kubernetes config map V1 data
***********************************/
resource "kubernetes_config_map_v1_data" "config_map_v1_data" {
  dynamic "metadata" {
    for_each = var.metadata

    content {
      name      = metadata.value.name
      namespace = lookup(metadata.value, "namespace", null)
    }
  }
  data  = var.data
  force = var.force
}