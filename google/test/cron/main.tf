variable "cron" {
  type    = string
  default = "0 23 * * *"
}

locals {
  cron_list = split(" ", var.cron)
  weekly    = length(split("/", local.cron_list[4])) > 1 ? "weekly" : ""
  monthly   = length(split("/", local.cron_list[2])) > 1 ? "monthly" : ""
  daily     = local.cron_list[0] != "*" && local.cron_list[1] != "*" && local.cron_list[2] == "*" && local.cron_list[3] == "*" && local.cron_list[4] == "*" ? "Daily" : ""
  type      = coalesce(local.weekly, local.monthly, local.daily)
}

output "type" {
  value = local.type
}
