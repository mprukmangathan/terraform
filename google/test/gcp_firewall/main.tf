locals {
  firewall_rules = [
    {
      name        = "gke-intra-cluster-egress"
      description = "Managed by terraform gke module: Allow pods to communicate with each other and the master"
      priority    = 1000
      direction   = "EGRESS"
      target_tags = ["gke"]
      ranges      = ["10.0.0.0/16"]
      allow = [
        {
          protocol = "tcp"
          ports    = []
        },
        {
          protocol = "udp"
          ports    = []
        },
        {
          protocol = "icmp"
          ports    = []
        },
        {
          protocol = "sctp"
          ports    = []
        },
        {
          protocol = "esp"
          ports    = []
        },
        {
          protocol = "ah"
          ports    = []
        }
      ]
      log_config = {
        metadata = "INCLUDE_ALL_METADATA"
      }
    }
  ]
}

provider "google" {
  credentials = file("credentials.json")
  region      = "us-central1"
  project     = "dazzling-matrix-361211"
}

module "gke_firewall_rules" {
  source   = "terraform-google-modules/network/google//modules/firewall-rules"
  for_each = { for rules in local.firewall_rules : rules.name => rules }

  project_id   = "dazzling-matrix-361211"
  network_name = "default"
  rules = [
    {
      name                    = each.value.name
      description             = each.value.description
      direction               = lookup(each.value, "direction", null)
      priority                = lookup(each.value, "priority", null)
      ranges                  = lookup(each.value, "ranges", [])
      source_tags             = lookup(each.value, "source_tags", null)
      source_service_accounts = lookup(each.value, "source_service_accounts", null)
      target_tags             = lookup(each.value, "target_tags", null)
      target_service_accounts = lookup(each.value, "target_service_accounts", null)
      allow                   = lookup(each.value, "allow", [])
      deny                    = lookup(each.value, "deny", [])
      log_config              = lookup(each.value, "log_config", {})
    }
  ]
}
