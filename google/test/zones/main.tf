provider "google" {
  credentials = file("../credentials.json")
  region      = "us-central1"
}

data "google_compute_zones" "zones" {
  project = "dazzling-matrix-361211"
  region  = var.region
}

variable "region" {
  default = "us-central1"
}

variable "cluster_zones" {
  default = ["us-central1-a", "us-central1-b"]

  validation {
    condition = can([for zone in distinct(var.cluster_zones) : regex("^us-", zone)])
    error_message = "The clusters zones must be part of same region."
  }
  /*
  validation {
    condition = length(distinct(var.cluster_zones)) > 1
    error_message = "The clusters zones should have more than one zone."
  }
*/
}
/*
output "zones" {
  value = var.cluster_zones #flatten(local.cluster_zones)
}
*/


locals {
  cluster_zones = try(flatten([for zone in distinct(var.cluster_zones) : regex(format("^(%s)$", join("|", data.google_compute_zones.zones.names)), zone)]), "The cluster zones are not part of cluster region")
}

output "zones" {
  value = local.cluster_zones
}
