locals {
  keepers = {
    short_name = var.short_name
    purpose = var.purpose
    region = var.region
  }
}

provider "google" {
  credentials = file("../../creds/credentials.json")
  region      = "us-central1"
}

data "google_compute_zones" "available" {
  project = "dazzling-matrix-361211"
  region  = var.region
}

resource "random_shuffle" "az" {
  keepers      = local.keepers
  input        = data.google_compute_zones.available.names
  result_count = 1
}

output "zone" {
  value = random_shuffle.az.result[0]
}

variable "short_name" {
  default = "us"
}

variable "purpose" {
  default = "test"
}

variable "region" {
  default = "us-central1"
}

output "zones" {
  value = [data.google_compute_zones.available.names[0], data.google_compute_zones.available.names[1]]
}