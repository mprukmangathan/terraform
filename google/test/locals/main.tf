provider "google" {
  credentials = file("../credentials.json")
  region      = "us-central1"
  project     = "dazzling-matrix-361211"
}

locals {
  twenty    = "prukmangathan2020@gmail.com"
  twentytwo = "prukmangathan22@gmail.com"
  arun      = "arunruku@gmail.com"
}

module "project_iam" {
  source  = "../../modules/iam/project-iam"
  project = "dazzling-matrix-361211"
  bindings = {
    #"roles/dataproc.worker" = flatten([local.myids])
    "roles/dataproc.worker"  = flatten([local.myids])
    "roles/metastore.editor" = ["user:${local.arun}"]
  }
}