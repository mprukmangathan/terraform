
resource "null_resource" "update_firewall_rule" {

  provisioner "local-exec" {
    command     = "gcloud compute firewall-rules update k8s-fw-a94510548619a4560bfc4ce91ddb7343 --source-ranges=35.235.240.0/20 --project=io1-taxservice-dev"
    interpreter = ["bash", "-c"]
  }
}



