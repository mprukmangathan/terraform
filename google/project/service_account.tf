resource "google_service_account" "iaac_service_account" {
  account_id   = "iaac-service-account-7482"
  display_name = "IaC Deployment"
  project      = google_project.pipeline.project_id
}

resource "google_service_account_iam_binding" "iaac_sa_user" {
  service_account_id = google_service_account.iaac_service_account.name
  role               = "roles/iam.serviceAccountUser"

  members = [
    "user:rukmangathanpuru@gmail.com",
  ]
}

resource "google_service_account_iam_binding" "iaac_sa_user_token_gen" {
  service_account_id = google_service_account.iaac_service_account.name
  role               = "roles/iam.serviceAccountTokenCreator"

  members = [
    "user:rukmangathanpuru@gmail.com",
  ]
}

resource "google_project_iam_member" "iaac_project_iam" {
  for_each = toset([
    "roles/compute.storageAdmin",
    "roles/compute.admin"
  ])
  member  = "serviceAccount:${google_service_account.iaac_service_account.email}"
  role    = each.key
  project = google_project.learn_new.project_id
}

resource "google_project_iam_member" "iaac_pipeline_iam" {
  for_each = toset([
    "roles/cloudbuild.integrationsOwner",
    "roles/cloudbuild.builds.editor",
    "roles/compute.storageAdmin"
  ])
  member  = "serviceAccount:${google_service_account.iaac_service_account.email}"
  role    = each.key
  project = google_project.pipeline.project_id
}
