provider "google" {
  credentials = file("../model-hexagon-351804-6b3b6f158f1e.json")
  region      = "us-central1"
  project     = "model-hexagon-351804"
}

provider "google-beta" {
  credentials = file("../model-hexagon-351804-6b3b6f158f1e.json")
  project     = "model-hexagon-351804"
  region      = "us-central1"
}

