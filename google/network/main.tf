resource "google_compute_firewall" "ssh_firewall_rule" {
  name          = "allow_ssh"
  network       = "default"
  description   = "allow ssh access to all"
  source_ranges = ["0.0.0.0/0"] # define ip ranges to restrict
  target_tags   = ["web"]

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
}