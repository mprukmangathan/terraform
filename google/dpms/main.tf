locals {
    metastore_primary = "projects/eastern-team-377416/locations/us-central1/services/metastore-primary"
    metastore_secondary = "projects/eastern-team-377416/locations/us-east4/services/metastore-secondary"
    control_file_object_url = join("/", ["gs:/", google_storage_bucket_object.control_file.bucket, google_storage_bucket_object.control_file.name])
}

data "template_file" "control_file" {
    template = templatefile("${path.module}/control_file.json", {
        active = local.metastore_primary
        standby = local.metastore_secondary
    })
}

resource "local_file" "control_file" {
    filename = "${path.module}/control_file_updated.json"
    content = data.template_file.control_file.rendered
}

resource "google_storage_bucket_object" "control_file" {
    name = "metastore_export_import_files/control_file.json"
    source = local_file.control_file.filename
    bucket = module.dpms.operations_bucket
}

module "dpms" {
    source = "../modules/metastore-replication"
    project_id = "eastern-team-377416"
    cluster_name = "hive"
    primary_metastore_service_id = local.metastore_primary
    secondary_metastore_service_id = local.metastore_secondary
    operations_bucket_url = local.control_file_object_url
}