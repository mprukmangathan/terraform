output "name" {
  value = google_cloudfunctions_function.main.name
}

output "id" {
  value = google_cloudfunctions_function.main.id
}