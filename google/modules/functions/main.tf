locals {
  source_bucket = var.gcs_archive_path != "" ? regex("gs://(.*?)/(.*)", var.gcs_archive_path)[0] : null
  source_object = var.gcs_archive_path != "" ? regex("gs://(.*?)/(.*)", var.gcs_archive_path)[1] : null
  name          = "${var.prefix}-${var.app_id}-${var.environment}-${format("%.2s", random_id.uuid.dec)}"
}

resource "random_id" "uuid" {
  keepers = {
    seed = "${var.prefix}-${var.app_id}-${var.environment}"
  }
  byte_length = 8
}

data "google_project" "nums" {
  for_each   = toset(compact([for item in var.secret_environment_variables : lookup(item, "project_id", "")]))
  project_id = each.value
}

data "google_project" "default" {
  count      = length(var.secret_environment_variables) > 0 ? 1 : 0
  project_id = var.project_id
}

# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_binding" "developer" {
  project        = google_cloudfunctions_function.main.project
  region         = google_cloudfunctions_function.main.region
  cloud_function = google_cloudfunctions_function.main.name

  role    = "roles/cloudfunctions.developer"
  members = var.developers
}

resource "google_cloudfunctions_function" "main" {
  name                          = local.name
  description                   = var.description
  available_memory_mb           = var.available_memory_mb
  max_instances                 = var.max_instances
  timeout                       = var.timeout_s
  entry_point                   = var.entry_point
  ingress_settings              = var.ingress_settings
  vpc_connector_egress_settings = var.vpc_connector_egress_settings
  vpc_connector                 = var.vpc_connector

  event_trigger {
    event_type = var.event_type
    resource   = var.resource

    failure_policy {
      retry = var.event_trigger_failure_policy_retry
    }
  }

  dynamic "secret_environment_variables" {
    for_each = { for item in var.secret_environment_variables : item.key => item }

    content {
      key        = secret_environment_variables.value["key"]
      project_id = try(data.google_project.nums[secret_environment_variables.value["project_id"]].number, data.google_project.default[0].number)
      secret     = secret_environment_variables.value["secret_name"]
      version    = lookup(secret_environment_variables.value, "version", "latest")
    }
  }

  labels                      = var.labels
  runtime                     = var.runtime
  environment_variables       = var.environment_variables
  source_archive_bucket       = local.source_bucket
  source_archive_object       = local.source_object
  project                     = var.project_id
  region                      = var.region
  service_account_email       = var.service_account_email
  build_environment_variables = var.build_environment_variables
}