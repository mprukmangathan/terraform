provider "google" {
  credentials = file("../../../creds/credentials.json")
  region      = "us-central1"
  project     = var.project 
}

resource "google_service_account" "function_sa" {
  account_id   = "bqbar-functions"
  display_name = "bqbar functions service account"
  project = var.project
}

resource "google_project_iam_member" "source" {
  project = var.project
  role    = "roles/bigquery.admin"
  member  = "serviceAccount:${google_service_account.function_sa.email}"
}

resource "google_project_iam_member" "target" {
  project = var.target_project
  role    = "roles/bigquery.admin"
  member  = "serviceAccount:${google_service_account.function_sa.email}"
}

resource "google_bigquery_table" "source" {
  project = var.project
  dataset_id = "source_bq_ds"
  table_id   = "datafolks"
  deletion_protection = false
  schema = file("data/schema.json")
}

resource "random_id" "int" {
  keepers = {
    first = "${timestamp()}"
  }     
  byte_length = 8
}

resource "google_storage_bucket" "bigquery_data" {
  name     = "bq-bar-table-data-7482"
  location = "US"
}

resource "google_storage_bucket_object" "bigquery_data_upload" {
  name   = "sample.csv"
  source = "${path.module}/data/sample.csv"
  bucket = google_storage_bucket.bigquery_data.name
}

resource "google_bigquery_job" "job" {
  project = var.project
  job_id = format("%s_%.2s", "load_job", random_id.int.dec)
  load {
    source_uris = [
      join("/", ["gs:/", google_storage_bucket_object.bigquery_data_upload.bucket, google_storage_bucket_object.bigquery_data_upload.name])
    ]
    destination_table {
      project_id = google_bigquery_table.source.project
      dataset_id = google_bigquery_table.source.dataset_id
      table_id   = google_bigquery_table.source.table_id
    }
    skip_leading_rows = 0
    schema_update_options = ["ALLOW_FIELD_RELAXATION", "ALLOW_FIELD_ADDITION"]
    write_disposition = "WRITE_APPEND"
  }
}

module "bqbar" {
  source = "../"
  project = var.project
  target_project = var.target_project
  source_dataset_name = "source_bq_ds"
  target_dataset_name = "target_bq_ds"
  functions_sa = "serviceAccount:${google_service_account.function_sa.email}"

  depends_on = [
    google_bigquery_job.job
  ]
}