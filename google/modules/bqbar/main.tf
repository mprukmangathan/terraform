locals {
    data = <<EOT
    {
      "source_dataset_name": "${var.source_dataset_name}",
      "target_dataset_name": "${var.target_dataset_name}",
      "crontab_format": "*/10 * * * *",
      "seconds_before_expiration": "${var.seconds_before_expiration}",
      "tables_to_include_list": "${jsonencode(var.tables_to_include_list)}",
      "tables_to_exclude_list": "${jsonencode(var.tables_to_exclude_list)}"
    }
    EOT
    target_project = var.target_project == "" ? var.project : var.target_project
    functions_sa_email = element(split(":", var.functions_sa), 1)
}

# Cloud function source code bucket
resource "google_storage_bucket" "cloud_function_code_backup" {
  name     = "bq-bar-dev-7482"
  location = "US"
}
resource "google_storage_bucket_iam_member" "cloud_function_code_backup_iam" {
  bucket = google_storage_bucket.cloud_function_code_backup.name
  role   = "roles/storage.admin"
  member = var.functions_sa
}

# Get table topic
resource "google_pubsub_topic" "table_function_topic" {
  name = "bq-bar-get-tables"
}
resource "google_pubsub_topic_iam_member" "table_function_topic_iam" {
  topic = google_pubsub_topic.table_function_topic.name
  role = "roles/pubsub.publisher"
  member = var.functions_sa
}
resource "google_pubsub_subscription" "table_function_sub" {
  name  = "bq-bar-get-tables-sub"
  topic = google_pubsub_topic.table_function_topic.name
}
resource "google_pubsub_subscription_iam_member" "table_function_sub_iam" {
  subscription = google_pubsub_subscription.table_function_sub.id
  role         = "roles/pubsub.subscriber"
  member       = var.functions_sa
}

# Cloud Scheduler
resource "google_cloud_scheduler_job" "trigger_pubsub_topic" {
  name     = "trigger-bq-bar-get-tables-topic"
  schedule = "*/10 * * * *"
  region = "us-central1"
  time_zone = "Asia/Calcutta"

  pubsub_target {
    topic_name = google_pubsub_topic.table_function_topic.id
    data       = base64encode(local.data)
  }

  depends_on = [
    google_pubsub_subscription_iam_member.table_function_sub_iam,
    google_pubsub_subscription_iam_member.backup_sub_iam
  ]
}

# backup topic
resource "google_pubsub_topic" "backup_topic" {
  name = "bq-bar-backup"
}
resource "google_pubsub_topic_iam_member" "backup_topic_iam" {
  topic = google_pubsub_topic.backup_topic.name
  role = "roles/pubsub.publisher"
  member = var.functions_sa
}
resource "google_pubsub_subscription" "backup_sub" {
  name  = "bq-bar-backup-sub"
  topic = google_pubsub_topic.backup_topic.name
}
resource "google_pubsub_subscription_iam_member" "backup_sub_iam" {
  subscription = google_pubsub_subscription.backup_sub.id
  role         = "roles/pubsub.subscriber"
  member       = var.functions_sa
}


resource "random_id" "random" {
  keepers = {
    first = "${timestamp()}"
  }     
  byte_length = 8
}

data "archive_file" "get_table" {
  type        = "zip"
  output_path = "${path.module}/data/temp/get_tables_${format("%.3s", random_id.random.dec)}.zip"
  source_dir = "${path.module}/data/get_tables/"
}

data "archive_file" "backup" {
  type        = "zip"
  output_path = "${path.module}/data/temp/backup_${format("%.3s", random_id.random.dec)}.zip"
  source_dir = "${path.module}/data/backup/"
}

# Uploading the code
resource "google_storage_bucket_object" "get_tables" {
  name   = "backup_${format("%.3s", random_id.random.dec)}.zip"
  bucket = google_storage_bucket.cloud_function_code_backup.name
  source = data.archive_file.get_table.output_path
}
resource "google_storage_bucket_object" "backup" {
  name   = "get_tables_${format("%.3s", random_id.random.dec)}.zip"
  bucket = google_storage_bucket.cloud_function_code_backup.name
  source = data.archive_file.backup.output_path
}

# Cloud function
resource "google_cloudfunctions_function" "get_tables" {
  name                  = "bq_bar_get_tables"
  runtime               = "python37"
  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.cloud_function_code_backup.name
  source_archive_object = google_storage_bucket_object.get_tables.name
  entry_point           = "main"
  ingress_settings      = "ALLOW_ALL"
  service_account_email = local.functions_sa_email
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = google_pubsub_topic.table_function_topic.id
  }
  environment_variables = {
    PUBSUB_TOPIC_ID = google_pubsub_topic.backup_topic.name
    PROJECT_ID = var.project
  }

  depends_on = [
    google_cloud_scheduler_job.trigger_pubsub_topic
  ]
}

# Cloud function
resource "google_cloudfunctions_function" "backup" {
  name                  = "bq_bar_backup"
  runtime               = "python37"
  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.cloud_function_code_backup.name
  source_archive_object = google_storage_bucket_object.backup.name
  entry_point           = "main"
  ingress_settings      = "ALLOW_ALL"
  service_account_email = local.functions_sa_email
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = google_pubsub_topic.backup_topic.id
  }
  environment_variables = {
    PROJECT_ID = local.target_project
  }
  
  depends_on = [
    google_cloud_scheduler_job.trigger_pubsub_topic
  ]
}
