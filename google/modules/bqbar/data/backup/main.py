import logging
from google.cloud import bigquery
from datetime import datetime
from dateutil.relativedelta import relativedelta
from cronsim import CronSim
import json
import logging
import time
import base64
import os

PROJECT_ID = os.environ.get('PROJECT_ID', 'Specified environment variable is not set.')

def get_snapshot_timestamp(message):
    cron_format = message['crontab_format']
    it = CronSim(cron_format, datetime.now())
    next_interval = next(it)
    next_next_interval = next(it)
    delta = (next_next_interval - next_interval).total_seconds()
    prev_interval = next_interval - relativedelta(seconds=delta)
    prev_cron_interval_timestamp = int(prev_interval.timestamp() * 1000)
    return prev_cron_interval_timestamp

def create_snapshot(client, message):
    target_dataset_name = message['target_dataset_name']
    seconds_before_expiration = message['seconds_before_expiration']

    prev_cron_interval_timestamp = get_snapshot_timestamp(message)

    current_date = datetime.now().strftime("%Y%m%d")
    snapshot_expiration_date = datetime.now() + relativedelta(seconds=int(seconds_before_expiration))

    source_table_fullname = message['table_name']
    source_table_name = source_table_fullname.split(".")[2]
    snapshot_name = f"{PROJECT_ID}.{target_dataset_name}.{source_table_name}_{current_date}"
    source_table_fullname = f"{source_table_fullname}@{prev_cron_interval_timestamp}"

    job_config = bigquery.CopyJobConfig()
    job_config.operation_type = "SNAPSHOT"
    job_config._properties["copy"]["destinationExpirationTime"] = snapshot_expiration_date.strftime("%Y-%m-%dT%H:%M:%SZ")
    job = client.copy_table(source_table_fullname, snapshot_name, job_config=job_config)
    logging.info(f"Creating snapshot for table: {snapshot_name}")
    return job

def main(event, context):
    message = base64.b64decode(event['data']).decode('utf-8')
    message = json.loads(message)

    client = bigquery.Client(project = PROJECT_ID)
    job = create_snapshot(client, message)

    while True:
        if job.done():
            exception = job.exception()
            if exception:
                logging.info(set(exception))
                raise Exception(str(exception))
            else:
                return 'ok'
        time.sleep(2)