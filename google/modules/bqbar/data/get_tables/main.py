import logging
import json
import base64
import os
from google.cloud import bigquery
from google.cloud import pubsub_v1

TABLE_TYPE_PHYSICAL_TABLE = "TABLE"
PUBSUB_TOPIC_ID = os.environ.get('PUBSUB_TOPIC_ID', 'Specified environment variable is not set.')
PROJECT_ID = os.environ.get('PROJECT_ID', 'Specified environment variable is not set.')

def filter_tables(tables, message):
    tables_to_include_list = message.get("tables_to_include_list", [])
    tables_to_exclude_list = message.get("tables_to_exclude_list", [])

    tables = [ x for x in tables if x.table_type == TABLE_TYPE_PHYSICAL_TABLE]
    if len(tables_to_include_list) > 0:
        tables = [x for x in tables if x.table_id in tables_to_include_list]
    if len(tables_to_exclude_list) > 0:
        tables = [x for x in tables if x.table_id in tables_to_exclude_list]
    
    tables = [f"{x.project}.{x.dataset_id}.{x.table_id}" for x in tables]

    return tables

def main(event, context):
    message = base64.b64decode(event['data']).decode('utf-8')
    message = json.loads(message)
    source_dataset = message['source_dataset_name']

    logging.info(f"Project ID: {PROJECT_ID}")
    logging.info(f"Source Dataset Name: {source_dataset}")
    logging.info(f"Pub/Sub Topic ID: {PUBSUB_TOPIC_ID}")

    publisher = pubsub_v1.PublisherClient()
    table_name_topic_path = publisher.topic_path(PROJECT_ID, PUBSUB_TOPIC_ID)

    client = bigquery.Client(project=PROJECT_ID)
    tables = client.list_tables(source_dataset)

    print("Tables: %s" %tables)

    tables = filter_tables(tables, message)

    for table_name in tables:
        logging.info(f"sending Pub/Sub message for table: {table_name}")
        message['table_name'] = table_name
        data = json.dumps(message)
        publisher.publish(table_name_topic_path, data.encode("utf-8"))

    return "ok"