variable "project" {
    type = string
}

variable "target_project" {
    type = string
}

variable "functions_sa" {
    type = string
}

variable "source_dataset_name" {
    type = string
}

variable "target_dataset_name" {
    type = string
}

variable "seconds_before_expiration" {
    type = number
    default = 604800
}

variable "tables_to_include_list" {
    type = list(string)
    default = []
}

variable "tables_to_exclude_list" {
    type = list(string)
    default = []
}