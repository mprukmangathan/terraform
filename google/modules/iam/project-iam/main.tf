/***********************************
  Locals
***********************************/

locals {
  bindings = flatten([
    for role, members in var.bindings :
    [for member in toset(members) : { role = role, member = member }]
  ])

  validation = [for binding in local.bindings : regex("^(user:|group:|serviceAccount:)", binding.member)]

  binding = { for k, v in local.bindings : k => v }
}


/***********************************
  Project-IAM
***********************************/

resource "google_project_iam_member" "project" {
  for_each = local.binding
  project  = var.project
  role     = each.value.role
  member   = each.value.member
} 