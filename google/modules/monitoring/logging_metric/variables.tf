variable "project_id" {
  type = string
}

variable "name" {
  type = string
}

variable "filter" {
  type = string
}

variable "description" {
  type    = string
  default = ""
}

variable "display_name" {
  type = string
}

variable "unit" {
  type    = string
  default = null
}

variable "metric_kind" {
  type = string
}

variable "value_type" {
  type = string
}

variable "value_extractor" {
  type    = string
  default = null
}

variable "labels" {
  type = list(object({
    key         = string
    value_type  = string
    description = string
    extractor   = string
  }))
  default = []
}