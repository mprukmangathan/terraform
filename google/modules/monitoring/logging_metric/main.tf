locals {
  label_extractors = { for value in var.labels : value.key => value.extractor if(length(var.labels) > 0) }
}

resource "google_logging_metric" "logging_metric" {
  name    = var.name
  filter  = var.filter
  project = var.project_id

  metric_descriptor {
    metric_kind  = var.metric_kind
    value_type   = var.value_type
    unit         = var.unit
    display_name = var.display_name

    dynamic "labels" {
      for_each = var.labels
      content {
        key         = labels.value.key
        value_type  = labels.value.value_type
        description = labels.value.description
      }
    }
  }

  value_extractor  = var.value_extractor
  label_extractors = local.label_extractors

}