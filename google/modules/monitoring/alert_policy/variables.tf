variable "duration_minutes" {
  type    = number
  default = 1
}

variable "project_id" {
  type = string
}

variable "combiner" {
  type    = string
  default = "OR"
}

variable "notification_channels" {
  type    = list(string)
  default = []
}

variable "display_name" {
  type = string
}

variable "metric_id" {
  type = string
}

variable "comparison" {
  type    = string
  default = "COMPARISON_GT"
}

variable "threshold_value" {
  type    = number
  default = 1
}

variable "user_labels" {
  type    = map(string)
  default = {}
}

variable "email_address" {
  type    = string
  default = ""
}

variable "create_notification_channel" {
  type    = bool
  default = false
}

variable "per_series_aligner" {
  type    = string
  default = "ALIGN_COUNT"
}