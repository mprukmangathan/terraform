locals {
  duration_sec    = var.duration_minutes * 60
  duration_string = "${local.duration_sec}s"
}

resource "google_monitoring_alert_policy" "alert_policy" {
  display_name          = format("%s_%s", var.display_name, "alert_policy")
  combiner              = var.combiner
  notification_channels = var.create_notification_channel ? [google_monitoring_notification_channel.notification_channel[0].name] : var.notification_channels
  project               = var.project_id

  conditions {
    display_name = format("%s_%s", var.display_name, "condition")
    condition_threshold {
      filter          = "metric.type=\"logging.googleapis.com/user/${var.metric_id}\" AND resource.type=\"gce_instance\""
      duration        = local.duration_string
      comparison      = var.comparison
      threshold_value = var.threshold_value
      aggregations {
        alignment_period   = local.duration_string
        per_series_aligner = var.per_series_aligner
      }
    }
  }

  user_labels = var.user_labels
}

resource "google_monitoring_notification_channel" "notification_channel" {
  count        = var.create_notification_channel ? 1 : 0
  display_name = format("%s_%s", var.display_name, "notification_channel")
  project      = var.project_id
  type         = "email"
  labels = {
    email_address = var.email_address
  }
  force_delete = false
}