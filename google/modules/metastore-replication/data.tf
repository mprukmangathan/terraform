/***************************
 Functions Code
***************************/
data "archive_file" "dpms_export" {
  type        = "zip"
  output_path = "${path.module}/functions-code/temp/dpms_export.zip"
  source_dir  = "${path.module}/functions-code/dpms_export/"
}

data "archive_file" "dpms_import" {
  type        = "zip"
  output_path = "${path.module}/functions-code/temp/dpms_import.zip"
  source_dir  = "${path.module}/functions-code/dpms_import/"
}