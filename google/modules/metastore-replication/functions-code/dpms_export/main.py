import base64
import google.cloud.logging
from google.cloud import storage
from google.cloud import metastore_v1
import json
import logging
from inspect import currentframe
from datetime import datetime
import os
import asyncio

GCS_OUTPUT_EXPORT_FILE_NAME = f'metastore_export_import_files/export/export_{datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")}.txt'
RESPONSE_KEY_NAME = f'name' 
GCS_BUCKET_URL = os.environ.get('operationsBucketUrl', 'specified env variable is not set')
GCS_BUCKET_NAME_STRING = GCS_BUCKET_URL.split("/")[2]
GCS_CONTROL_FILE_NAME = GCS_BUCKET_URL.split("/", 3)[-1]
ACTIVE_KEY_NAME = f'active'
VALUE_MISMATCH = f'Service Id mismatch with metastore service ID found in control file.'
GCS_STORAGE_CLIENT = storage.Client()
GCS_BUCKET = GCS_STORAGE_CLIENT.get_bucket(GCS_BUCKET_NAME_STRING)

client = google.cloud.logging.Client()
client.setup_logging()

def read_control_file():
	blob = GCS_BUCKET.blob(GCS_CONTROL_FILE_NAME)
	control_file_data = json.loads(blob.download_as_string(client=None))
	fetched_metadata_server_name = control_file_data[ACTIVE_KEY_NAME]
	logging.info(f'Active metastore service ID from control file JSON : {fetched_metadata_server_name}')
	return fetched_metadata_server_name
	
def proceed(service_id):
	return True if service_id.lower().strip() == read_control_file().lower().strip() else False
	
def RaiseException(ex):
	cf = currentframe()
	line = cf.f_back.f_lineno
	logging.exception('Traceback:\n'+ex+'\nin line '+str(line))
	raise SystemExit()
	
def create_gcs_entry_file(response_json):
	content = response_json[RESPONSE_KEY_NAME]
	blob = GCS_BUCKET.blob(GCS_OUTPUT_EXPORT_FILE_NAME)
	
	with blob.open("w") as f:
		f.write(content)
		
def dpms_export(event, context):
	pubsub_message = base64.b64decode(event['data']).decode('utf-8')
	request_json = json.loads(pubsub_message)
	logging.info(f'pub/sub message: {request_json}')
	serviceId = request_json["serviceId"]
	clusterName = request_json["cluster_name"]
	destinationGcsFolder = request_json["destinationGcsFolder"]
	databaseDumpType = request_json["databaseDumpType"]
	
	decision = proceed(service_id=serviceId)
	if not decision:
		message = f'{VALUE_MISMATCH} . for dataproc cluster {clusterName}, {serviceId} is not the active metastore for export. Hence, aborting.'
		control_file_data = json.loads(GCS_BUCKET.blob(GCS_CONTROL_FILE_NAME).download_as_string(client=None))
		controlFileMessage = f'control File contents:\n {control_file_data}'
		logging.info(message)
		logging.info(controlFileMessage)
		return message
		
	asyncio.run(submit_export_job(clusterName, destinationGcsFolder, serviceId, databaseDumpType))
	
async def submit_export_job(clusterName, destinationGcsFolder, serviceId, databaseDumpType):
	client = metastore_v1.DataprocMetastoreAsyncClient()
	request = metastore_v1.ExportMetadataRequest(
		destination_gcs_folder=destinationGcsFolder,
		service=serviceId,
		database_dump_type=databaseDumpType
	)
	operation = await client.export_metadata(request=request)
	logging.info(f'The export operation has started on {serviceId} for dataproc cluster {clusterName}, writting to {destinationGcsFolder}.')
	logging.info(f'operation: {operation}')
	logging.info(f'request: {request}')
	
	return operation