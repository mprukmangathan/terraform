import base64
import google.cloud.logging
from google.cloud import storage
from google.cloud import metastore_v1
import json
import logging
from inspect import currentframe
from datetime import datetime
import os
import asyncio
import time
import rstr

GCS_OUTPUT_EXPORT_FILE_NAME = f'metastore_export_import_files/import/import_{datetime.now().strftime("%Y_%m_%d-%I_%M_%S_%p")}.txt'
RESPONSE_KEY_NAME = f'name' 
GCS_BUCKET_URL = os.environ.get('operationsBucketUrl', 'specified env variable is not set')
GCS_BUCKET_NAME_STRING = GCS_BUCKET_URL.split("/")[2]
GCS_CONTROL_FILE_NAME = GCS_BUCKET_URL.split("/", 3)[-1]
DR_KEY_NAME = f'standby'
VALUE_MISMATCH = f'Service Id mismatch with metastore service ID found in control file.'
GCS_STORAGE_CLIENT = storage.Client()
GCS_BUCKET = GCS_STORAGE_CLIENT.get_bucket(GCS_BUCKET_NAME_STRING)

client = google.cloud.logging.Client()
client.setup_logging()

def read_control_file():
	blob = GCS_BUCKET.blob(GCS_CONTROL_FILE_NAME)
	control_file_data = json.loads(blob.download_as_string(client=None))
	fetched_metadata_server_name = control_file_data[DR_KEY_NAME]
	logging.info(f'Active metastore service ID from control file JSON : {fetched_metadata_server_name}')
	return fetched_metadata_server_name
	
	
def RaiseException(ex):
	cf = currentframe()
	line = cf.f_back.f_lineno
	logging.exception('Traceback:\n'+ex+'\nin line '+str(line))
	raise SystemExit()
	
def create_gcs_entry_file(response_json):
	content = response_json[RESPONSE_KEY_NAME]
	blob = GCS_BUCKET.blob(GCS_OUTPUT_EXPORT_FILE_NAME)
	
	with blob.open("w") as f:
		f.write(content)
		
def dpms_import(event, context):
	logging.info(f'Storage event: {event}')
	sourceGcsFolder = "gs://"+event['bucket']+"/"+event['name']
	logging.info(f'source gcs folder: {sourceGcsFolder}')
	
	database_dump = metastore_v1.types.MetadataImport(
		database_dump = metastore_v1.types.MetadataImport.DatabaseDump(gcs_uri = sourceGcsFolder)
	)
	
	serviceId = read_control_file().strip()
	logging.info(f'service id: {serviceId}')
	
	asyncio.run(submit_import_job(serviceId, database_dump))
	
async def submit_import_job(serviceId, database_dump):
	client = metastore_v1.DataprocMetastoreAsyncClient()
	request = metastore_v1.CreateMetadataImportRequest(
		parent=serviceId,
		metadata_import_id=gen_name(),
		metadata_import = database_dump_file
	)
	operation = await client.create_metadata_import(request=request)
	logging.info(f'The import operation has started on {serviceId}, reading from {database_dump_file}.')
	logging.info(f'operation: {operation}')
	
	return operation
	
def gen_name():
	import_name = 'import-' + rstr.xeger(r'[a-z]{2}[0-9]{5}')
	return import_name