/***************************
 API
***************************/
module "dpms_api" {
  source                      = "terraform-google-modules/project-factory/google//modules/project_services"
  version                     = "14.1.0"
  project_id                  = var.project_id
  disable_services_on_destroy = false
  activate_apis = [
    "metastore.googleapis.com",
    "storage.googleapis.com",
    "pubsub.googleapis.com",
    "cloudscheduler.googleapis.com",
    "cloudfunctions.googleapis.com",
    "run.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "iam.googleapis.com",
    "cloudbuild.googleapis.com"
  ]
}

resource "random_id" "uuid" {
  keepers = {
    seed = "dpms-replica-${local.environment}"
  }
  byte_length = 8
}

/***************************
 Service Account
***************************/
module "dpms_sa" {
  source      = "terraform-google-modules/service-accounts/google"
  version     = "3.0"
  project_id  = var.project_id
  prefix      = "dpms"
  names       = ["replica"]
  description = "Service account for dataproc metastore replication"
  project_roles = [
    "${var.project_id}=>roles/storage.objectViewer",
    "${var.project_id}=>roles/metastore.editor",
    "${var.project_id}=>roles/cloudfunctions.serviceAgent"
  ]

  depends_on = [
    module.dpms_api
  ]
}

/***************************
 Storage buckets
***************************/
resource "google_storage_bucket" "dpms_buckets" {
  for_each      = toset(["functions-code", "exports-active", "exports-standby", "operations"])
  project       = var.project_id
  name          = "${local.prefix}-${each.key}-${local.suffix}"
  location      = local.dual_region
  force_destroy = true
  labels = {
    env = local.environment
  }
  custom_placement_config {
    data_locations = [
      upper(local.region),
      upper(local.secondary_region),
    ]
  }
  versioning {
    enabled = false
  }
  depends_on = [
    module.dpms_api
  ]
}

resource "google_storage_bucket_iam_member" "bucket_iam" {
  for_each = toset(["functions-code", "exports-active", "exports-standby", "operations"])
  bucket   = google_storage_bucket.dpms_buckets[each.key].name
  role     = "roles/storage.admin"
  member   = module.dpms_sa.iam_email
}

/***************************
 PubSub Topics
***************************/
resource "google_pubsub_topic" "dpms_topics" {
  for_each = toset(["active", "standby"])
  project  = var.project_id
  name     = "${local.prefix}-${each.key}-${local.suffix}"
}
resource "google_pubsub_topic_iam_member" "dpms_topic_iam" {
  for_each = toset(["active", "standby"])
  project  = var.project_id
  topic    = google_pubsub_topic.dpms_topics[each.key].name
  role     = "roles/pubsub.publisher"
  member   = module.dpms_sa.iam_email
}

/***************************
 Cloud Scheduler
***************************/
resource "google_cloud_scheduler_job" "dpms_active" {
  name      = "${local.prefix}-scheduler-primary-${local.suffix}"
  project   = var.project_id
  schedule  = var.cron
  region    = local.region
  time_zone = var.time_zone

  pubsub_target {
    topic_name = google_pubsub_topic.dpms_topics["active"].id
    data       = base64encode(local.active_scheduler_data)
  }
}

resource "google_cloud_scheduler_job" "dpms_standby" {
  name      = "${local.prefix}-scheduler-secondary-${local.suffix}"
  project   = var.project_id
  schedule  = var.cron
  region    = local.secondary_region
  time_zone = var.time_zone

  pubsub_target {
    topic_name = google_pubsub_topic.dpms_topics["standby"].id
    data       = base64encode(local.standby_scheduler_data)
  }
}

/***************************
 Functions Code
***************************/
resource "google_storage_bucket_object" "export" {
  name   = "dpms_export.zip"
  bucket = google_storage_bucket.dpms_buckets["functions-code"].name
  source = data.archive_file.dpms_export.output_path
}

resource "google_storage_bucket_object" "import" {
  name   = "dpms_import.zip"
  bucket = google_storage_bucket.dpms_buckets["functions-code"].name
  source = data.archive_file.dpms_import.output_path
}

/***************************
 Cloud Functions
***************************/
resource "google_cloudfunctions_function" "export_active" {
  name                  = "${local.prefix}-export-active-${local.suffix}"
  project               = var.project_id
  region                = local.region
  runtime               = "python37"
  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.dpms_buckets["functions-code"].name
  source_archive_object = google_storage_bucket_object.export.name
  entry_point           = "dpms_export"
  ingress_settings      = "ALLOW_ALL"
  service_account_email = module.dpms_sa.email
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = google_pubsub_topic.dpms_topics["active"].id
  }
  environment_variables = {
    operationsBucketUrl = var.operations_bucket_url
  }

  depends_on = [
    google_cloud_scheduler_job.dpms_active
  ]
}

resource "google_cloudfunctions_function" "export_standby" {
  name                  = "${local.prefix}-export-standby-${local.suffix}"
  project               = var.project_id
  region                = local.secondary_region
  runtime               = "python37"
  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.dpms_buckets["functions-code"].name
  source_archive_object = google_storage_bucket_object.export.name
  entry_point           = "dpms_export"
  ingress_settings      = "ALLOW_ALL"
  service_account_email = module.dpms_sa.email
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = google_pubsub_topic.dpms_topics["standby"].id
  }
  environment_variables = {
    operationsBucketUrl = var.operations_bucket_url
  }

  depends_on = [
    google_cloud_scheduler_job.dpms_standby
  ]
}

resource "google_cloudfunctions_function" "import_active" {
  name                  = "${local.prefix}-import-active-${local.suffix}"
  project               = var.project_id
  region                = local.region
  runtime               = "python37"
  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.dpms_buckets["functions-code"].name
  source_archive_object = google_storage_bucket_object.import.name
  entry_point           = "dpms_import"
  ingress_settings      = "ALLOW_ALL"
  service_account_email = module.dpms_sa.email
  event_trigger {
    event_type = "google.storage.object.finalize"
    resource   = google_storage_bucket.dpms_buckets["exports-standby"].name
  }
  environment_variables = {
    operationsBucketUrl = var.operations_bucket_url
  }
}

resource "google_cloudfunctions_function" "import_standby" {
  name                  = "${local.prefix}-import-standby-${local.suffix}"
  project               = var.project_id
  region                = local.secondary_region
  runtime               = "python37"
  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.dpms_buckets["functions-code"].name
  source_archive_object = google_storage_bucket_object.import.name
  entry_point           = "dpms_import"
  ingress_settings      = "ALLOW_ALL"
  service_account_email = module.dpms_sa.email
  event_trigger {
    event_type = "google.storage.object.finalize"
    resource   = google_storage_bucket.dpms_buckets["exports-active"].name
  }
  environment_variables = {
    operationsBucketUrl = var.operations_bucket_url
  }
}