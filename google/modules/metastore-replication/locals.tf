locals {
  region                 = var.region == "" ? "us-central1" : var.region
  secondary_region       = var.secondary_region == "" ? "us-east4" : var.secondary_region
  dual_region            = "${upper(local.region)}+${upper(local.secondary_region)}"
  environment            = var.environment
  prefix                 = "dpms-replica"
  suffix                 = "${local.environment}${format("%.2s", random_id.uuid.dec)}"
  active_scheduler_data  = <<-EOT
    {
      "cluster_name": "${var.cluster_name}",
      "serviceId": "${var.primary_metastore_service_id}",
      "databaseDumpType": "MYSQL",
      "destinationGcsFolder": "${google_storage_bucket.dpms_buckets["exports-active"].url}"
    }
    EOT
  standby_scheduler_data = <<-EOT
    {
      "cluster_name": "${var.cluster_name}",
      "serviceId": "${var.secondary_metastore_service_id}",
      "databaseDumpType": "MYSQL",
      "destinationGcsFolder": "${google_storage_bucket.dpms_buckets["exports-standby"].url}"
    }
    EOT
}