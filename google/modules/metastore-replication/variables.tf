variable "environment" {
  type    = string
  default = "dev"
}

variable "region" {
  type    = string
  default = ""
}

variable "secondary_region" {
  type    = string
  default = ""
}

variable "cluster_name" {
  type = string
}

variable "primary_metastore_service_id" {
  type = string
}

variable "secondary_metastore_service_id" {
  type = string
}

variable "project_id" {
  type = string
}

variable "time_zone" {
  type    = string
  default = "Asia/Calcutta"
}

variable "cron" {
  type    = string
  default = "0 23 * * *"
}

variable "operations_bucket_url" {
  type = string
}