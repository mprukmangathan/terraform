terraform {
  backend "gcs" {
    bucket = "ce-vault-tf-state-dev"
    prefix = "terraform.tfstate"
  }
}

locals {
  username = "ansibleuser"
}

provider "vault" {
  address = "http://9e1d-27-5-103-77.ngrok.io"
  token   = "s.7i76jODA3caeGY1Z3l4ytlCx"
}

data "google_compute_image" "services_test_2019" {
  family  = "sql-std-2019-win-2019"
  project = "windows-sql-cloud"
}

resource "google_compute_address" "static" {
  name    = "windows-winrm"
  project = "learnnew-7482"
  region  = "us-east4"
}

resource "google_compute_instance" "services_test_2019" {
  project      = "learnnew-7482"
  name         = "services-test-2019"
  machine_type = "e2-standard-2"
  zone         = "us-east4-a"

  tags = ["services-test"]

  labels = {
    ansible_config = "services-test"
  }

  #TO-DO: Update the Custom Metadata key to "sysprep-specialize-script-ps1" if you want to run the script at initial boot.
  # windows-startup-script-ps1 for subsequent reboot
  #source: https://cloud.google.com/compute/docs/instances/startup-scripts/windows#metadata-keys
  metadata = {
    sysprep-specialize-script-ps1 = <<EOT
#Upgrading PowerShell and .NET Framework
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$url = "https://raw.githubusercontent.com/jborean93/ansible-windows/master/scripts/Upgrade-PowerShell.ps1"
$file = "$env:temp\Upgrade-PowerShell.ps1"
$username = "Administrator"
$password = "Password"
(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force
# Version can be 3.0, 4.0 or 5.1
&$file -Version 5.1 -Username $username -Password $password -Verbose
# This isn't needed but is a good security practice to complete
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Force
$reg_winlogon_path = "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon"
Set-ItemProperty -Path $reg_winlogon_path -Name AutoAdminLogon -Value 0
Remove-ItemProperty -Path $reg_winlogon_path -Name DefaultUserName -ErrorAction SilentlyContinue
Remove-ItemProperty -Path $reg_winlogon_path -Name DefaultPassword -ErrorAction SilentlyContinue

# Install the OpenSSH Server
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
# Start the sshd service
Start-Service sshd
# OPTIONAL but recommended:
Set-Service -Name sshd -StartupType 'Automatic'
# Confirm the Firewall rule is configured. It should be created automatically by setup. Run the following to verify
if (!(Get-NetFirewallRule -Name "OpenSSH-Server-In-TCP" -ErrorAction SilentlyContinue | Select-Object Name, Enabled)) {
    Write-Output "Firewall Rule 'OpenSSH-Server-In-TCP' does not exist, creating it..."
    New-NetFirewallRule -Name 'OpenSSH-Server-In-TCP' -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22
} else {
    Write-Output "Firewall rule 'OpenSSH-Server-In-TCP' has been created and exists."
}

# WinRM Setup
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
$file = "$env:temp\ConfigureRemotingForAnsible.ps1"
(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
powershell.exe -ExecutionPolicy ByPass -File $file

# Winrm Listener
Set-Item -Path WSMan:\localhost\Service\Auth\Basic -Value $true
winrm quickconfig -transport:https
winrm enumerate winrm/config/Listener
EOT
  }

  boot_disk {
    initialize_params {
      image = data.google_compute_image.services_test_2019.self_link
      size  = "100"
      type  = "pd-ssd"
    }
  }

  allow_stopping_for_update = true

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.static.address
    }
  }

  service_account {
    email  = "1001338819825-compute@developer.gserviceaccount.com"
    scopes = ["cloud-platform"]
  }

}

# create windows admin useraccount and generate password using bash script
data "external" "windows_auth" {
  program = ["sh", "${path.module}/scripts/windows_auth.sh"]
  query = {
    GCE_ZONE          = "us-east4-a"
    PROJECT_ID        = "learnnew-7482"
    INSTANCE_HOSTNAME = google_compute_instance.services_test_2019.name
  }
}

#write the windows auth password generated in external data resource to the vault
resource "vault_generic_secret" "ansible_secret" {
  path      = "secret/${google_compute_instance.services_test_2019.name}"
  data_json = <<EOT
    {
      "username": "${local.username}",
      "password": "${data.external.windows_auth.result.password}"
     }
  EOT
}

output "password" {
  # sensitive = true
  value = data.external.windows_auth.result
}
