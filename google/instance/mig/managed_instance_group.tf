data "google_compute_image" "my_image" {
  family  = "debian-9"
  project = "debian-cloud"
}

resource "google_compute_instance_template" "igm-basic" {
  name           = "my-template"
  machine_type   = "e2-medium"
  can_ip_forward = false
  tags           = ["foo", "bar"]

  disk {
    source_image = data.google_compute_image.my_image.self_link
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network    = google_compute_network.default.id
    subnetwork = google_compute_subnetwork.default.id
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  metadata = {
    startup-script = <<-EOF
       #! /bin/bash
       sudo apt-get update
       sudo apt-get install apache2 -y
       echo "<html><h1>\"Hostname: $(hostname)\"</h1></html>" > /var/www/html
       EOF
  }
}

resource "google_compute_instance_group_manager" "igm-no-tp" {
  description = "Terraform test instance group manager"
  name        = "my-igm"

  version {
    name              = "prod"
    instance_template = google_compute_instance_template.igm-basic.self_link
  }

  base_instance_name = "igm-no-tp"
  zone               = "us-central1-a"
  target_size        = 2
}

resource "google_compute_address" "default" {
  name         = "website-ip-1"
  provider     = google-beta
  region       = "us-central1"
  network_tier = "STANDARD"
}
