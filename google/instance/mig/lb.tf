resource "google_compute_forwarding_rule" "default" {
  name                  = "website-forwarding-rule"
  region                = "us-central1"
  load_balancing_scheme = "INTERNAL_MANAGED"
  port_range            = "80"
  backend_service       = google_compute_region_backend_service.backend.id
  all_ports             = true
  allow_global_access   = true
  network               = google_compute_network.default.name
  subnetwork            = google_compute_subnetwork.default.name
}

resource "google_compute_region_backend_service" "backend" {
  name = "website-backend"
  backend {
    group           = google_compute_region_instance_group_manager.rigm.instance_group
    balancing_mode  = "UTILIZATION"
    capacity_scaler = 1.0
  }
  region        = "us-central1"
  health_checks = [google_compute_health_check.hc.id]
}

resource "google_compute_health_check" "hc" {
  name               = "check-website-backend"
  check_interval_sec = 1
  timeout_sec        = 1
  tcp_health_check {
    port = "80"
  }
}
