#!/bin/sh

#set -x

# Uncomment when running locally

# INSTANCE_HOSTNAME="services-test-2019-0a8ed346"
# GCE_ZONE="us-east4-a"
# PROJECT_ID="devops-175ef596"

sleep 30

# Comment when running locally
eval "$(jq -r '@sh "INSTANCE_HOSTNAME=\(.INSTANCE_HOSTNAME) GCE_ZONE=\(.GCE_ZONE) PROJECT_ID=\(.PROJECT_ID)"')"
WORKING_DIR="/tmp/windows_auth"

# Validate the windows-keys metadata on windows instances
#METADATA=$(gcloud compute instances describe ${INSTANCE_HOSTNAME} --zone ${GCE_ZONE} --format="value(metadata)"  | grep "windows-keys")
#if [ -z $METADATA ]
#then
USERNAME="ansibleuser"
USER_EMAIL="ansibleuser@appliedsystems.com"
EXPIRE_DATE=$(date --date='now +5 minutes' +%Y-%m-%dT%H:%M:%SZ)
PATH="/usr/local/gcloud/google-cloud-sdk/bin:${PATH}"

mkdir -p $WORKING_DIR
cd $WORKING_DIR

openssl genrsa -out private_key 2048 > /dev/null 2>&1
openssl rsa -pubout -in private_key -out public_key > /dev/null 2>&1

MODULUS=$(cat $WORKING_DIR/public_key | grep -v -- ----- | openssl enc -base64 -d | dd bs=1 skip=33 count=256 2>/dev/null | base64 -w 0; echo)
EXPONENT=$(cat $WORKING_DIR/public_key | grep -v -- ----- | openssl enc -base64 -d | dd bs=1 skip=291 count=3 2>/dev/null | openssl enc -base64)

printf '{"userName": "%s", "modulus": "%s", "exponent": "%s", "expireOn": "%s", "email": "%s"}' "${USERNAME}" "${MODULUS}" "${EXPONENT}" "${EXPIRE_DATE}" "${USER_EMAIL}" > $WORKING_DIR/metadata_payload.json

if [[ -f $WORKING_DIR/metadata_payload.json ]];
then
  gcloud compute instances add-metadata ${INSTANCE_HOSTNAME} --project ${PROJECT_ID} --zone ${GCE_ZONE} --metadata-from-file="windows-keys=metadata_payload.json" > /dev/null
else
  echo "Payload file not found."
fi

ENCRYPTED_PASSWORD=`gcloud compute instances get-serial-port-output ${INSTANCE_HOSTNAME} --project=${PROJECT_ID} --zone=${GCE_ZONE} --port=4 2>/dev/null | jq -r -c --arg MODULUS $MODULUS 'select(.modulus == $MODULUS) | .encryptedPassword'`
WIN_USERPASSWORD=$(echo $ENCRYPTED_PASSWORD | openssl enc -base64 -d | openssl rsautl -decrypt -inkey private_key -oaep)

printf '{"Project": "%s", "Hostname": "%s", "userName": "%s", "password": "%s", "modulus": "%s", "exponent": "%s", "expireOn": "%s", "email": "%s"}' "${PROJECT_ID}" "${INSTANCE_HOSTNAME}" "${USERNAME}" "${WIN_USERPASSWORD}" "${MODULUS}" "${EXPONENT}" "${EXPIRE_DATE}" "${USER_EMAIL}"