data "template_file" "init" {
  template = templatefile("control_file.json", {
    active = var.active
    dr     = var.dr
  })
}

resource "local_file" "control_file" {
  filename = "${path.module}/control_file_temp.json"
  content  = data.template_file.init.rendered
}

resource "google_storage_bucket_object" "control_file" {
  name   = "control_file/control_file.json"
  source = local_file.control_file.filename
  bucket = "gcs-bucket-metastore-srv-74ac0a53-9e13-46a3-89cf-7d045f579ddd"
}

variable "active" {
  default = "project/one/metastore/one"
}

variable "dr" {
  default = "project/two/metastore/two"
}

output "control_file" {
  value = data.template_file.init.rendered
}