provider "google" {
  credentials = file("model-hexagon-351804-cf77cda108b3.json")
  region      = "us-central1"
  project     = "model-hexagon-351804"
}
