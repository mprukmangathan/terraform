provider "google" {
  credentials = file("../../test/credentials.json")
  region      = "us-central1"
  project     = "dazzling-matrix-361211"
}