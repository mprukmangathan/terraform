locals {
  gcs_archive_path = join("/", ["gs:/", google_storage_bucket.cloud_function_code_backup.name, google_storage_bucket_object.archive.name])
}

# Cloud function service account
resource "google_service_account" "functions" {
  project      = "dazzling-matrix-361211"
  account_id   = "cfsa-7482"
  display_name = "cfsa-7482"
}

# Pub/Sub for Cloud Scheduler
# Pub/Sub Topic
resource "google_pubsub_topic" "cloud_scheduler_topic" {
  name = "cloud_scheduler_topic"
}

# Pub/Sub Subscription
resource "google_pubsub_subscription" "cloud_scheduler_sub" {
  name  = "cloud_scheduler_subscription"
  topic = google_pubsub_topic.cloud_scheduler_topic.name
}

# Cloud Scheduler
resource "google_cloud_scheduler_job" "trigger_cloud_function" {
  name     = "trigger_cloud_function"
  schedule = "*/10 * * * *"

  pubsub_target {
    topic_name = google_pubsub_topic.cloud_scheduler_topic.id
    data       = base64encode("{\"function_name\":\"sample_function\", \"table_name\":\"names_2014\"}")
  }
}

# Cloud function source code bucket
resource "google_storage_bucket" "cloud_function_code_backup" {
  name     = "gcp-cloud-functions-code-7482"
  location = "US"
}

# Uploading the code
resource "google_storage_bucket_object" "archive" {
  name   = "index.zip"
  bucket = google_storage_bucket.cloud_function_code_backup.name
  source = "${path.module}/data/code/index.zip"
}

# Cloud function
module "cloud_functions" {
  project_id  = "dazzling-matrix-361211"
  region      = "us-central1"
  source      = "../../modules/functions"
  prefix      = "tax"
  environment = "dev"
  app_id      = "loans"

  gcs_archive_path    = local.gcs_archive_path
  available_memory_mb = 128
  runtime             = "python37"
  entry_point         = "hello_gcp"

  event_type            = "google.pubsub.topic.publish"
  resource              = google_pubsub_topic.cloud_scheduler_topic.id
  service_account_email = google_service_account.functions.email
}