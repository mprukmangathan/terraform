provider "google" {
  credentials = file("../model-hexagon-351804-cf77cda108b3.json")
  region      = "us-central1"
  project     = "model-hexagon-351804"
}

provider "google-beta" {
  credentials = file("../model-hexagon-351804-cf77cda108b3.json")
  project     = "model-hexagon-351804"
  region      = "us-central1"
}

resource "random_string" "suffix" {
  length  = 4
  special = "false"
  upper   = "false"
}

data "google_compute_image" "my_image" {
  family  = "debian-11"
  project = "debian-cloud"
}

module "vpc" {
  source       = "../modules/networking/vpc"
  network_name = "demo-vpc-03"
  project_id   = "model-hexagon-351804"
  mtu          = 1460
}

module "subnet" {
  source       = "../modules/networking/subnet"
  network_name = module.vpc.network_name
  project_id   = "model-hexagon-351804"

  subnets = [
    {
      subnet_name   = "subnet-03"
      subnet_ip     = "10.101.10.0/24"
      subnet_region = "us-central1"
    }
  ]
}

module "cloud-nat" {
  source        = "../modules/cloud-nat"
  create_router = true
  router        = "test-router"
  project_id    = "model-hexagon-351804"
  region        = "us-central1"
  name          = "my-cloud-nat"
  network       = module.vpc.network_name
}

/** Instance Template **/

module "instance_template" {
  source     = "../modules/instance-template"
  project_id = "model-hexagon-351804"
  #network              = module.vpc.network_name
  subnetwork           = module.subnet.self_link["subnet-03"]
  tags                 = ["web", "http-server", "https-server"]
  source_image         = data.google_compute_image.my_image.name
  source_image_project = "debian-cloud"
  auto_delete          = true
  metadata = {
    startup-script = "${file("./configure.sh")}"
  }
  service_account = {
    email  = "iaac-sa@model-hexagon-351804.iam.gserviceaccount.com",
    scopes = ["cloud-platform"]
  }
}

/** Instance Group within autoscale and health check **/

module "mig" {
  source              = "../modules/mig"
  project_id          = "model-hexagon-351804"
  instance_template   = module.instance_template.self_link
  region              = "us-central1"
  autoscaling_enabled = true
  min_replicas        = 2
  autoscaler_name     = "mig-as"
  hostname            = "mig-as"

  autoscaling_cpu = [
    {
      target = 0.4
    },
  ]

  health_check_name = "mig-https-hc"
  health_check = {
    type                = "http"
    initial_delay_sec   = 120
    check_interval_sec  = 5
    healthy_threshold   = 2
    timeout_sec         = 5
    unhealthy_threshold = 2
    response            = ""
    proxy_header        = "NONE"
    port                = 80
    request             = ""
    request_path        = "/"
    host                = "localhost"
  }
}