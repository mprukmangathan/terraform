provider "google" {
  credentials = file("../../test/credentials.json")
  region      = "us-central1"
  project     = "dazzling-matrix-361211"
}

provider "google" {
  credentials = file("../../test/credentials.json")
  region      = "us-east1"
  project     = "dazzling-matrix-361211"
  alias       = "east"
}