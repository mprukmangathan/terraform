provider "google" {
  credentials = file("../../test/credentials.json")
  region      = "us-central1"
}


module "logging_metric" {
  source       = "../../modules/monitoring/logging_metric"
  name         = "sovos_logs/metric"
  display_name = "sovos_logs"
  project_id   = "dazzling-matrix-361211"
  metric_kind  = "DELTA"
  value_type   = "INT64"
  filter       = "resource.type=\"gce_instance\" AND resource.labels.instance_id=\"3011294088000726260\" AND textPayload=\"TESTING... Server state changed from ALIVE to DEAD\" OR textPayload=\"TESTING... Server state changed from STARTED to DEAD\""
  labels = [
    {
      key         = "alive_to_dead"
      value_type  = "STRING"
      description = "monitor the alive dead logs"
      extractor   = "REGEXP_EXTRACT(textPayload, \"(.*ALIVE to DEAD)\")"
    },
    {
      key         = "started_to_dead"
      value_type  = "STRING"
      description = "monitor the alive dead logs"
      extractor   = "REGEXP_EXTRACT(textPayload, \"(.*STARTED to DEAD)\")"
    }
  ]
}

module "alert_policy" {
  source                      = "../../modules/monitoring/alert_policy"
  display_name                = "sovos_logs"
  project_id                  = "dazzling-matrix-361211"
  create_notification_channel = true
  email_address               = "prukmangathan2020@gmail.com"
  metric_id                   = module.logging_metric.id
  duration_minutes            = 5
}