variable "notification_channels" {
  type    = list(string)
  default = ["compute_instance_sovos_logs"]
}