module "compute_instance" {
  source              = "../../modules/compute_instance/"
  region              = "us-central1"
  subnetwork          = var.subnetwork
  num_instances       = 1
  hostname            = "instance-simple"
  instance_template   = module.instance_template.self_link
  deletion_protection = false

  access_config = [{
    nat_ip       = var.nat_ip
    network_tier = var.network_tier
  }, ]
}