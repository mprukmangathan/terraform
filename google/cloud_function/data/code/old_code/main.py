def hello_gcs(event, context):
    from google.cloud import pubsub_v1
    import time
    import json
    import os

    PUB_SUB_TOPIC = "cloud_function_topic"
    PUB_SUB_PROJECT = os.environ.get('GCP_PROJECT')

    publisher = pubsub_v1.PublisherClient() 
    payload = {"EventId": context.event_id, "EventType": context.event_type, "Bucket": event['bucket'], "File": event['name'], "timestamp": event['timeCreated']}
    print(f"Sending payload: {payload}.")
    data = json.dumps(payload).encode("utf-8")
    topic_path = publisher.topic_path(PUB_SUB_PROJECT, PUB_SUB_TOPIC)
    future = publisher.publish(topic_path, data=data)
    print("Pushed message to topic.")
    #print('Event ID: {}'.format(context.event_id))
    #print('Event type: {}'.format(context.event_type))
    #print('Bucket: {}'.format(event['bucket']))
    #print('File: {}'.format(event['name']))
    #print('Metageneration: {}'.format(event['metageneration']))
    #print('Created: {}'.format(event['timeCreated']))
    #print('Updated: {}'.format(event['updated']))