# Function dependencies, for example: 
# package>=version 
google-cloud-storage==1.35.0 
google-cloud-bigquery==1.19.0 
google-cloud-core==1.5.0 
pandas~=1.1.5 
pandas-datareader 
gcsfs==0.6.2 
google-cloud-pubsub==2.2.0 
configparser
protobuf==3.20.*