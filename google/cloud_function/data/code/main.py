class EmailMetrics:
    def __init__(self, fq_table_name, row_count_sql, row_count_bucket):
        self.FULLY_QUALIFIED_TABLE_NAME = fq_table_name.upper()
        self.ROW_COUNT_FROM_SQL = row_count_sql
        self.ROW_COUNT_FROM_BUCKET = row_count_bucket

def process_table_retention(event, context):
    from google.cloud import bigquery
    from google.cloud import storage
    from configparser import RawConfigParser
    from datetime import datetime
    import json
    import base64
    import os
    
    project_id, topic_id_metrics, dataset_name, table_name, storage_bucket, row_count_from_sql, row_count_from_bucket = "", "", "", "", "", 0, 0
    
    try:
        print("Table Retention Process Started")
        pubsub_message = base64.b64decode(event['data']).decode('utf-8')
        print(pubsub_message)
        j = json.loads(pubsub_message)
        print(j)
        function_name = os.environ.get("FUNCTION_NAME")
        print("function_name: %s" %function_name)
        if function_name != str(j['function_name']):
            return
        # Assigning parameters
        config = RawConfigParser()
        config.read("./config.ini")
        print("assigning parameters")
        project_id = config['TABLE_RETENTION']['Project_ID']
        topic_id_metrics = config['TABLE_RETENTION']['Topic_ID_For_Metrics']
        dataset_name = config['TABLE_RETENTION']['Dataset']
        table_name = j['table_name']
        storage_bucket = config['TABLE_RETENTION']['Bucket_Name']
        print("project_id: %s, topic_id_metrics: %s, dataset_name: %s, table_name: %s, storage_bucket: %s" %(project_id, topic_id_metrics, dataset_name, table_name, storage_bucket))

        client = bigquery.Client(project_id)
        select_query = """SELECT name,count FROM `""" + project_id + """.""" + dataset_name + """.""" + table_name \
                        + """` WHERE gender = 'M' ORDER BY count DESC LIMIT 5"""
        job_config = bigquery.QueryJobConfig(priority=bigquery.QueryPriority.BATCH)
        select_query_job = client.query(select_query, job_config=job_config)
        row_count_from_sql = select_query_job.result().total_rows
        print("Number of records pulled from sql:", row_count_from_sql)
        rows_df = select_query_job.result().to_dataframe()
        storage_client = storage.Client(project_id)
        bucket = storage_client.get_bucket(storage_bucket)
        print("Storing in bucket:", format(bucket))
        filename = table_name + '_' + datetime.now().strftime("%Y%m%d%H%M%S") + '.csv'
        blob = bucket.blob(filename)
        blob.upload_from_string(rows_df.to_csv(sep=',', index=False, encoding='utf-8'),content_type='text/csv')
        dataframe_from_csv = get_csv_count(storage_bucket, filename, project_id)
        row_count_from_bucket = len(dataframe_from_csv)
        print("Number of records loaded on bucket:", row_count_from_bucket)
        full_qualified_table_name = project_id + "." + dataset_name + "." + table_name
        data_to_publish = [EmailMetrics(full_qualified_table_name, row_count_from_sql, row_count_from_bucket).__dict__]
        metrics_to_publish = {"identifier": "BQ_TABLE_COUNT", "data": data_to_publish}
        message_id = publish_message_to_pubsub(project_id, topic_id_metrics, metrics_to_publish)
        print("Pubsub published message id for bq table count:", message_id)
    except Exception as e:
        print("Exception on process_table_retention" + str(e))
        return 0

def get_csv_count(bucket_name, csv_name, proj_id):
    import gcsfs
    import pandas as pd
    fs = gcsfs.GCSFileSystem(proj_id)
    with fs.open(bucket_name + '/' + csv_name) as f:
        df = pd.read_csv(f, delimiter=",")
        return df

def publish_message_to_pubsub(proj_id, top_id, message):
    from google.cloud import pubsub_v1
    import json
    SENDER_INFO_FOR_PUBSUB = "bq_operations"
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(proj_id, top_id)
    try:
        request_json = json.dumps(message)
        data = request_json.encode('utf-8')
        future = publisher.publish(topic_path, data, origin=SENDER_INFO_FOR_PUBSUB, username="admin")
        message_id = future.result()
        print("pubsub message id: " + message_id)
        return message_id
    except Exception as e:
        print("Exception on publish_message_to_pubsub" + str(e))
        return 0