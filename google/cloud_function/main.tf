provider "google" {
  credentials = file("model-hexagon-351804-6b3b6f158f1e.json")
  region      = "us-central1"
  project     = "model-hexagon-351804"
}

# Pub/Sub for Cloud Scheduler
# Pub/Sub Topic
resource "google_pubsub_topic" "cloud_scheduler_topic" {
  name = "cloud_scheduler_topic"
}

# Pub/Sub Subscription
resource "google_pubsub_subscription" "cloud_scheduler_sub" {
  name  = "cloud_scheduler_subscription"
  topic = google_pubsub_topic.cloud_scheduler_topic.name
}

# Pub/Sub for Cloud Function
# Pub/Sub Topic
resource "google_pubsub_topic" "cloud_function_topic" {
  name = "cloud_function_topic"
}

# Pub/Sub Subscription
resource "google_pubsub_subscription" "cloud_function_sub" {
  name  = "cloud_function_subscription"
  topic = google_pubsub_topic.cloud_function_topic.name
}

# Cloud Scheduler
resource "google_cloud_scheduler_job" "trigger_cloud_function" {
  name     = "trigger_cloud_function"
  schedule = "*/10 * * * *"

  pubsub_target {
    topic_name = google_pubsub_topic.cloud_scheduler_topic.id
    data       = base64encode("{\"function_name\":\"bq_operations\", \"table_name\":\"names_2014\"}")
  }
}

# Cloud function source code bucket
resource "google_storage_bucket" "cloud_function_code_backup" {
  name     = "bq-operations-dev-7482"
  location = "US"
}

# Uploading the code
resource "google_storage_bucket_object" "archive" {
  name   = "index.zip"
  bucket = google_storage_bucket.cloud_function_code_backup.name
  source = "./data/code/index.zip"
}

# uploading bq data
/*
resource "google_storage_bucket_object" "bq_data" {
  name   = "names_2014.txt"
  bucket = google_storage_bucket.cloud_function_code_backup.name
  source = "./data/bq_data/names_2014.txt"
}
*/

# Cloud function
resource "google_cloudfunctions_function" "function" {
  name                  = "bq_operations"
  runtime               = "python37"
  available_memory_mb   = 256
  source_archive_bucket = google_storage_bucket.cloud_function_code_backup.name
  source_archive_object = google_storage_bucket_object.archive.name
  entry_point           = "process_table_retention"
  ingress_settings      = "ALLOW_ALL"
  service_account_email = "gcf-7482@model-hexagon-351804.iam.gserviceaccount.com"
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = google_pubsub_topic.cloud_scheduler_topic.id
  }
  environment_variables = {
    FUNCTION_NAME = "bq_operations"
  }
  depends_on = [google_cloud_scheduler_job.trigger_cloud_function]
}
