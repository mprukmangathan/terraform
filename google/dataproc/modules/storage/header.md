# Google Cloud Storage

A module for creating cloud storage bucket

### Example

```hcl
module "storage_bucket" {
  source          = "./modules/storage"
  name            = "dataproc-test-bucket-01234"
  project         = "hexa-beta-7485"
  location        = "us-central1-a"
  object_admins   = "user:admin@example.com"
  object_viewers  = "group:viewers@example.com"
  lifecycle_rules = [
    {
      action    = { type = "Delete" }
      condition = { age = 30 } //days
    }
   ]
  }
}
```