<!-- BEGIN_TF_DOCS -->
# Google Cloud Storage

A module for creating cloud storage bucket

### Example

```hcl
module "storage_bucket" {
  source          = "./modules/storage"
  name            = "dataproc-test-bucket-01234"
  project         = "hexa-beta-7485"
  location        = "us-central1-a"
  object_admins   = "user:admin@example.com"
  object_viewers  = "group:viewers@example.com"
  lifecycle_rules = [
    {
      action    = { type = "Delete" }
      condition = { age = 30 } //days
    }
   ]
  }
}
```

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_storage_bucket.bucket](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket) | resource |
| [google_storage_bucket_iam_binding.authoritative](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_binding) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_lifecycle_rules"></a> [lifecycle\_rules](#input\_lifecycle\_rules) | n/a | `list(any)` | `[]` | no |
| <a name="input_location"></a> [location](#input\_location) | n/a | `string` | `""` | no |
| <a name="input_name"></a> [name](#input\_name) | n/a | `string` | `""` | no |
| <a name="input_object_admins"></a> [object\_admins](#input\_object\_admins) | n/a | `list` | `[]` | no |
| <a name="input_object_creators"></a> [object\_creators](#input\_object\_creators) | n/a | `list` | `[]` | no |
| <a name="input_object_viewers"></a> [object\_viewers](#input\_object\_viewers) | n/a | `list` | `[]` | no |
| <a name="input_project"></a> [project](#input\_project) | n/a | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_name"></a> [name](#output\_name) | n/a |
| <a name="output_url"></a> [url](#output\_url) | n/a |
<!-- END_TF_DOCS -->