/***********************************
    Locals
***********************************/
locals {
  bindings = {
    "roles/storage.objectAdmin"   = setunion(flatten(var.object_admins))
    "roles/storage.objectCreator" = setunion(flatten(var.object_creators))
    "roles/storage.objectViewer"  = setunion(flatten(var.object_viewers))
  }
}

/***********************************
    Google Storage Bucket
***********************************/
resource "google_storage_bucket" "bucket" {
  name          = var.name
  project       = var.project
  location      = var.location
  force_destroy = true

  lifecycle {
    ignore_changes = [name]
  }

  dynamic "lifecycle_rule" {
    for_each = var.lifecycle_rules
    content {
      action {
        type          = lookup(lifecycle_rule.value.action, "type", null)
        storage_class = lookup(lifecycle_rule.value.action, "storage_class", null)
      }
      condition {
        age                    = lookup(lifecycle_rule.value.condition, "age", null)
        created_before         = lookup(lifecycle_rule.value.condition, "created_before", null)
        with_state             = lookup(lifecycle_rule.value.condition, "with_state", null)
        matches_storage_class  = lookup(lifecycle_rule.value.condition, "matches_storage_class", null)
        num_newer_versions     = lookup(lifecycle_rule.value.condition, "num_newer_versions", null)
        days_since_custom_time = lookup(lifecycle_rule.value.condition, "days_since_custom_time", null)
      }
    }
  }
}

/***********************************
    Storage IAM bindings
***********************************/
resource "google_storage_bucket_iam_binding" "authoritative" {
  for_each = local.bindings
  bucket   = google_storage_bucket.bucket.name
  role     = each.key
  members  = each.value
}

