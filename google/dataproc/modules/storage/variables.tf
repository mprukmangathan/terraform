variable "name" {
  type    = string
  default = ""
}

variable "project" {
  type    = string
  default = ""
}

variable "location" {
  type    = string
  default = ""
}

variable "lifecycle_rules" {
  type    = list(any)
  default = []
}

variable "object_admins" {
  default = []
}

variable "object_creators" {
  default = []
}

variable "object_viewers" {
  default = []
}