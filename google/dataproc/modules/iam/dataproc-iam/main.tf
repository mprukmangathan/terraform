/***********************************
  Dataproc IAM
***********************************/
resource "google_dataproc_cluster_iam_binding" "cluster_iam_binding" {
  cluster = var.cluster
  project = var.project
  region  = var.region
  role    = var.role
  members = var.members
}