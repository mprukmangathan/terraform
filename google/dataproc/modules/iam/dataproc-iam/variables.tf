variable "cluster" {
  type    = string
  default = ""
}

variable "project" {
  type    = string
  default = ""
}

variable "region" {
  type    = string
  default = ""
}

variable "role" {
  type    = string
  default = ""
}

variable "members" {
  type    = list(string)
  default = []
}
