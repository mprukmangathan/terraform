<!-- BEGIN_TF_DOCS -->
# Google Dataproc Autoscaling Policy

A module for creating a Dataproc autoscaling policy

### Example

```hcl
module "dataproc_autoscaling_policy" {
    source                        = "./modules/autoscaling"
    project                       = "hexa-beta-7485"
    policy_id                     = "sample_policy"
    location                      = "us-central1-a"
    max_instances                 = 3
    min_instances                 = 2
    secondary_max_instances       = 3
    secondary_min_instances       = 2
    graceful_decommission_timeout = "600s"
}
```

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_dataproc_autoscaling_policy.policy](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dataproc_autoscaling_policy) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cooldown_period"></a> [cooldown\_period](#input\_cooldown\_period) | n/a | `string` | `"2m"` | no |
| <a name="input_graceful_decommission_timeout"></a> [graceful\_decommission\_timeout](#input\_graceful\_decommission\_timeout) | n/a | `string` | `"30s"` | no |
| <a name="input_location"></a> [location](#input\_location) | n/a | `string` | `""` | no |
| <a name="input_max_instances"></a> [max\_instances](#input\_max\_instances) | n/a | `number` | `2` | no |
| <a name="input_min_instances"></a> [min\_instances](#input\_min\_instances) | n/a | `number` | `2` | no |
| <a name="input_policy_id"></a> [policy\_id](#input\_policy\_id) | n/a | `string` | `""` | no |
| <a name="input_project"></a> [project](#input\_project) | n/a | `string` | `""` | no |
| <a name="input_scale_down_factor"></a> [scale\_down\_factor](#input\_scale\_down\_factor) | n/a | `number` | `0.5` | no |
| <a name="input_scale_down_min_worker_fraction"></a> [scale\_down\_min\_worker\_fraction](#input\_scale\_down\_min\_worker\_fraction) | n/a | `number` | `0` | no |
| <a name="input_scale_up_factor"></a> [scale\_up\_factor](#input\_scale\_up\_factor) | n/a | `number` | `0.5` | no |
| <a name="input_scale_up_min_worker_fraction"></a> [scale\_up\_min\_worker\_fraction](#input\_scale\_up\_min\_worker\_fraction) | n/a | `number` | `0` | no |
| <a name="input_secondary_max_instances"></a> [secondary\_max\_instances](#input\_secondary\_max\_instances) | n/a | `number` | `20` | no |
| <a name="input_secondary_min_instances"></a> [secondary\_min\_instances](#input\_secondary\_min\_instances) | n/a | `number` | `2` | no |
| <a name="input_secondary_weight"></a> [secondary\_weight](#input\_secondary\_weight) | n/a | `number` | `0` | no |
| <a name="input_weight"></a> [weight](#input\_weight) | n/a | `number` | `0` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_id"></a> [id](#output\_id) | n/a |
| <a name="output_name"></a> [name](#output\_name) | n/a |
<!-- END_TF_DOCS -->