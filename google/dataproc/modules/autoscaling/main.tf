/***********************************
  Dataproc autoscaling policy
***********************************/
resource "google_dataproc_autoscaling_policy" "policy" {
  policy_id = var.policy_id
  location  = var.location
  project   = var.project

  worker_config {
    max_instances = var.max_instances
    min_instances = var.min_instances
    weight        = var.weight
  }

  secondary_worker_config {
    max_instances = var.secondary_max_instances
    min_instances = var.secondary_min_instances
    weight        = var.secondary_weight
  }

  basic_algorithm {
    cooldown_period = var.cooldown_period
    yarn_config {
      graceful_decommission_timeout  = var.graceful_decommission_timeout #"30s"
      scale_up_factor                = var.scale_up_factor               #0.5
      scale_down_factor              = var.scale_down_factor             #0.5
      scale_up_min_worker_fraction   = var.scale_up_min_worker_fraction
      scale_down_min_worker_fraction = var.scale_down_min_worker_fraction
    }
  }
}