variable "graceful_decommission_timeout" {
  type    = string
  default = "30s"
}

variable "project" {
  type    = string
  default = ""
}

variable "max_instances" {
  type    = number
  default = 2
}

variable "min_instances" {
  type    = number
  default = 2
}

variable "weight" {
  type    = number
  default = 0
}

variable "secondary_weight" {
  type    = number
  default = 0
}

variable "secondary_max_instances" {
  type    = number
  default = 20
}

variable "secondary_min_instances" {
  type    = number
  default = 2
}

variable "scale_up_factor" {
  type    = number
  default = 0.5
}

variable "scale_down_factor" {
  type    = number
  default = 0.5
}

variable "cooldown_period" {
  type    = string
  default = "2m"
}

variable "scale_up_min_worker_fraction" {
  type    = number
  default = 0.0
}

variable "scale_down_min_worker_fraction" {
  type    = number
  default = 0.0
}

variable "policy_id" {
  type    = string
  default = ""
}

variable "location" {
  type    = string
  default = ""
}
