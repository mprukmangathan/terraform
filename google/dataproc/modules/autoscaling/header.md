# Google Dataproc Autoscaling Policy

A module for creating a Dataproc autoscaling policy

### Example

```hcl
module "dataproc_autoscaling_policy" {
    source                        = "./modules/autoscaling"
    project                       = "hexa-beta-7485"
    policy_id                     = "sample_policy"
    location                      = "us-central1-a"
    max_instances                 = 3
    min_instances                 = 2
    secondary_max_instances       = 3
    secondary_min_instances       = 2
    graceful_decommission_timeout = "600s"
}
```