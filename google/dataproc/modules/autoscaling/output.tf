output "id" {
  value = google_dataproc_autoscaling_policy.policy.id
}

output "name" {
  value = google_dataproc_autoscaling_policy.policy.name
}