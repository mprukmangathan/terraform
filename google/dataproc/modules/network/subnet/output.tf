output "subnets" {
  value       = google_compute_subnetwork.subnetwork
  description = "The created subnet resources"
}

output "id" {
  value = {
    for k, v in google_compute_subnetwork.subnetwork : v.name => v.id
  }
  description = "The id of subnet resources"
}

output "self_link" {
  value = {
    for k, v in google_compute_subnetwork.subnetwork : v.name => v.self_link
  }
  description = "The subnet self link"
}
