variable "project" {
  type    = string
  default = ""
}

variable "admins" {
  default = []

  validation {
    condition     = can([for binding in setunion(flatten(var.admins)) : regex("^(user:|group:|serviceAccount:)", binding)])
    error_message = "IAM bindings must start with \"user:\", \"group:\", or \"serviceAccount:\"."
  }
}

variable "viewers" {
  default = []

  validation {
    condition     = can([for binding in setunion(flatten(var.viewers)) : regex("^(user:|group:|serviceAccount:)", binding)])
    error_message = "IAM bindings must start with \"user:\", \"group:\", or \"serviceAccount:\"."
  }
}

variable "developers" {
  default = []

  validation {
    condition     = can([for binding in setunion(flatten(var.developers)) : regex("^(user:|group:|serviceAccount:)", binding)])
    error_message = "IAM bindings must start with \"user:\", \"group:\", or \"serviceAccount:\"."
  }
}

variable "cluster_region" {
  type    = string
  default = "us-central1"
}

variable "application_name" {
  type    = string
  default = "demo"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "cluster_zones" {
  type    = list(string)
  default = ["us-central1-a", "us-central1-b"]
}

variable "service_account" {
  type    = string
  default = null
}

variable "service_account_scopes" {
  type    = list(string)
  default = ["cloud-platform"]
}

variable "multi_zone" {
  type    = bool
  default = false
}

variable "dataproc_service_account" {
  type    = string
  default = null
}

variable "metastore_service_account" {
  type    = string
  default = null
}

variable "metastore_service" {
  type    = bool
  default = false
}

variable "master_num_instances" {
  type    = number
  default = 1
}

variable "primary_num_instances" {
  type    = number
  default = 2
}

variable "secondary_num_instances" {
  type    = number
  default = 0
}

variable "master_machine_type" {
  type    = string
  default = "n1-standard-2"
}

variable "primary_machine_type" {
  type    = string
  default = "n1-standard-2"
}

variable "master_min_cpu_platform" {
  type    = string
  default = ""
}

variable "primary_min_cpu_platform" {
  type    = string
  default = ""
}

variable "metastore_allow_destroy" {
  type    = bool
  default = false
}

variable "master_boot_disk_type" {
  type    = string
  default = "pd-ssd"
}

variable "primary_boot_disk_type" {
  type    = string
  default = "pd-standard"
}

variable "secondary_boot_disk_type" {
  type    = string
  default = "pd-standard"
}

variable "master_boot_disk_size_gb" {
  type    = number
  default = 30
}

variable "primary_boot_disk_size_gb" {
  type    = number
  default = 20
}

variable "secondary_boot_disk_size_gb" {
  type    = number
  default = 20
}

variable "master_num_local_ssds" {
  type    = number
  default = 1
}

variable "primary_num_local_ssds" {
  type    = number
  default = 1
}

variable "secondary_num_local_ssds" {
  type    = number
  default = 1
}

variable "initialization_action_scripts" {
  type    = map(string)
  default = {}
}

variable "metastore_port" {
  type    = number
  default = 9080
}

variable "metastore_tier" {
  type    = string
  default = "DEVELOPER"
}

variable "metastore_maintenance_window_hour_of_day" {
  type    = number
  default = 2
}

variable "metastore_maintenance_window_day_of_week" {
  type    = string
  default = "SUNDAY"
}

variable "hive_metastore_version" {
  type    = string
  default = "3.1.2"
}

variable "metastore_config_overrides" {
  type    = map(string)
  default = {}
}

variable "software_override_properties" {
  type    = map(string)
  default = {}
}

variable "image_version" {
  type    = string
  default = "1.5.53-debian10"
}

variable "labels" {
  type    = map(any)
  default = {}
}

variable "autoscaling_policy_id" {
  type    = string
  default = ""
}

variable "preemptibility" {
  type    = string
  default = "PREEMPTIBLE"
}

variable "internal_ip_only" {
  type    = bool
  default = false
}

variable "tags" {
  type    = list(string)
  default = ["dataproc"]
}

variable "network" {
  type    = string
  default = null
}

variable "subnetwork" {
  type    = string
  default = null
}