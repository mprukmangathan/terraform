package test

import (
	"fmt"
	"path/filepath"
	"testing"
	"utils"

	"github.com/gruntwork-io/terratest/modules/terraform"
	test_structure "github.com/gruntwork-io/terratest/modules/test-structure"
	"github.com/stretchr/testify/assert"
)

func TestSimpleCluster(t, *testing.T) {

	exampleDir := "../example/simple-cluster"
	testDir := test_structure.CopyTerraformFolderToTemp(t, exampleDir, "")
	planFilePath := filepath.Join(testDir, fmt.Sprintf("%s.txt", "tfplan"))

	terraformOptions := &terraform.Options{
		NoColor:		true,
		PlanFilePath:	planFilePath,
		TerraformDir:	exampleDir,
		VarFiles:     []string{"simple_cluster.auto.tfvars"},
	}

	var out map[string]interface{}
	err := terraform.GetAllVariablesFromVarFileE(t, exampleDir+"/simple_cluster.auto.tfvars", &out)
	if err != nil {
		terraform.GetAllVariablesFromVarFile(t, exampleDir+"/simple_cluster.auto.tfvars", &out)
	}

	projectId := out["project"].map[string]interface{}
	app_name := out["application_name"].map[string]interface{}
	env := out["environment"].map[string]interface{}

	plan := terraform.InitAndPlanAndShowWithStruct(t, terraformOptions)
	defer utils.DumpErrors(t)

	expectedResources := []map[string]interface{}{
		{
			"address": `module.simple_cluster.google_dataproc_cluster.cluster`,
			"fields": utils.MSI{
				"project": projectId,
				"region": "us-central1",
				"cluster_config": []utils.MSI{
					{
						"master_config": []utils.MSI{
							{
								"num_instances": 1,
								"machine_type": "n1-standard-2",
							}
						},
						"worker_config": []utils.MSI{
							{
								"num_instances": 2,
								"machine_type": "n1-standard-2",
							}
						},
					}
				}

			}
		}
	}

	utils.CheckPlanExpectations(t, expectedResources, plan)
}