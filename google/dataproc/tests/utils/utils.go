package utils

import (
	"fmt"
	"github.com/gruntwork-io/terratest/modules/terraform"

	"regexp"
	"testing"
)

type MSI = map[string]interface{}

const horizontalLine = "*************************************"

var errorQueue = make([]error, 0)

func WatchForError(e error)  {
	if e != nil {
		errorQueue = append(errorQueue, e)
	}
}

func DumpErrors(t *testing.T)  {
	if len(errorQueue) > 0 {
		for len(errorQueue) > 0 {
			t.Log(errorQueue[0])
			errorQueue = errorQueue[1:]
		}
		t.FailNow()
	}
}