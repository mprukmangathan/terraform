
module "simple_cluster" {
  source                = "../../"
  application_name      = "tax"
  environment           = "qa"
  project               = var.project
  master_num_instances  = 1
  master_machine_type   = "n1-standard-2"
  primary_num_instances = 2
  primary_machine_type  = "n1-standard-2"
}

