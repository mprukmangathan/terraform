module "hive_metastore_cluster" {
  source                = "../../"
  application_name      = "spark"
  environment           = "dev"
  project               = var.project
  master_num_instances  = 1
  master_machine_type   = "n1-standard-2"
  primary_num_instances = 2
  primary_machine_type  = "n1-standard-2"
  software_override_properties = {
    "dataproc:dataproc.allow.zero.workers" = "true"
  }
  metastore_service = true
}