/***********************************
  Locals
***********************************/
locals {
  apis     = ["dataproc.googleapis.com", "metastore.googleapis.com"]
  location = element(split("-", var.cluster_region), 0)
  loc      = lower(local.location)
  name     = "${var.application_name}-${local.loc}-${var.environment}"

  admins     = setunion(flatten(var.admins))
  viewers    = setunion(flatten(var.viewers))
  developers = setunion(flatten(var.developers))

  dataproc_sa_fqn  = var.dataproc_service_account == null ? "serviceAccount:${google_service_account.dataproc[0].email}" : var.dataproc_service_account
  metastore_sa_fqn = var.metastore_service_account == null ? "serviceAccount:${google_project_service_identity.metastore[0].email}" : var.metastore_service_account

  cluster_zones = try(flatten([for zone in distinct(var.cluster_zones) : regex(format("^(%s)$", join("|", data.google_compute_zones.zones.names)), zone)]), "The cluster zones should be part of cluster region")
}

/***********************************
  Random string for resource name
***********************************/
resource "random_id" "name" {
  byte_length = 8
  keepers = {
    seed = "${var.application_name}-${local.loc}-${var.environment}"
  }
}

/***********************************
  Data Providers
***********************************/
data "google_compute_zones" "zones" {
  project = var.project
  region = var.cluster_region
  status = "UP"
}


/***********************************
  Activate Google API
***********************************/
resource "google_project_service" "activate_api" {
  for_each                   = toset(local.apis)
  project                    = var.project
  service                    = each.value
  disable_dependent_services = true
}

/***********************************
  IAM
***********************************/
resource "google_service_account" "dataproc" {
  count        = var.dataproc_service_account == null ? 1 : 0
  project      = var.project
  account_id   = "${local.name}-dataproc-sa-${format("%.2s", random_id.name.dec)}" #"dataproc-sa-iaac"
  display_name = "${local.name}-dataproc-sa-${format("%.2s", random_id.name.dec)}" #"dataproc-sa-iaac"
}

resource "google_project_service_identity" "metastore" {
  count    = var.metastore_service_account == null ? 1 : 0
  provider = google-beta
  project  = var.project
  service  = "metastore.googleapis.com"

  depends_on = [google_project_service.activate_api]
}

module "dataproc_project_iam" {
  source  = "./modules/iam/project-iam"
  project = var.project
  bindings = {
    "roles/dataproc.worker"  = [local.dataproc_sa_fqn]
    "roles/metastore.editor" = [local.dataproc_sa_fqn]
  }
}

module "metastore_project_iam" {
  source  = "./modules/iam/project-iam"
  count   = var.metastore_service ? 1 : 0
  project = var.project
  bindings = {
    "roles/metastore.serviceAgent" = [local.metastore_sa_fqn]
  }
}

module "dataproc_iam" {
  source  = "./modules/iam/dataproc-iam"
  count   = var.multi_zone ? length(local.cluster_zones) : 1
  project = google_dataproc_cluster.cluster[count.index].project
  region  = google_dataproc_cluster.cluster[count.index].region
  cluster = google_dataproc_cluster.cluster[count.index].name
  role    = "roles/dataproc.viewer"
  members = local.viewers
}

/***********************************
  Dataproc cloud storage buckets
***********************************/
module "staging_bucket" {
  source          = "./modules/storage"
  name            = "${local.name}-dataproc-staging-bucket-${format("%.4s", random_id.name.dec)}"
  project         = var.project
  location        = local.location
  object_admins   = flatten([local.admins, local.dataproc_sa_fqn])
  object_creators = local.developers
  object_viewers  = local.viewers
}

module "temp_bucket" {
  source          = "./modules/storage"
  name            = "${local.name}-dataproc-temp-bucket-${format("%.4s", random_id.name.dec)}"
  project         = var.project
  location        = local.location
  object_admins   = flatten([local.admins, local.dataproc_sa_fqn])
  object_creators = local.developers
  object_viewers  = local.viewers
  lifecycle_rules = [
    {
      action    = { type = "Delete" }
      condition = { age = 90 } //days
    }
  ]
}

module "metastore_bucket" {
  count           = var.metastore_service ? 1 : 0
  source          = "./modules/storage"
  name            = "${local.name}-dataproc-metastore-bucket-${format("%.4s", random_id.name.dec)}"
  project         = var.project
  location        = local.location
  object_admins   = flatten([local.admins, local.metastore_sa_fqn])
  object_creators = local.developers
  object_viewers  = local.viewers
}


/***********************************
  Dataproc Cluster
***********************************/
resource "google_dataproc_cluster" "cluster" {
  count   = var.multi_zone ? length(local.cluster_zones) : 1
  name    = var.multi_zone ? "${local.name}-dataproc-${local.cluster_zones[count.index]}-${format("%.2s", random_id.name.dec)}" : "${local.name}-dataproc-${format("%.2s", random_id.name.dec)}"
  project = var.project
  region  = var.cluster_region
  labels  = var.labels

  cluster_config {
    staging_bucket = module.staging_bucket.name
    temp_bucket    = module.temp_bucket.name

    autoscaling_config {
      policy_uri = var.autoscaling_policy_id
    }

    master_config {
      num_instances    = var.master_num_instances
      machine_type     = var.master_machine_type     #"n1-standard-2"
      min_cpu_platform = var.master_min_cpu_platform #"Intel Skylake"

      disk_config {
        boot_disk_type    = var.master_boot_disk_type    #"pd-ssd"
        boot_disk_size_gb = var.master_boot_disk_size_gb #30
        num_local_ssds    = var.master_num_local_ssds    #1
      }
    }

    worker_config {
      num_instances    = var.primary_num_instances
      machine_type     = var.primary_machine_type     #"n1-standard-2"
      min_cpu_platform = var.primary_min_cpu_platform #"Intel Skylake"

      disk_config {
        boot_disk_type    = var.primary_boot_disk_type    #"pd-standard"
        boot_disk_size_gb = var.primary_boot_disk_size_gb #30
        num_local_ssds    = var.primary_num_local_ssds    #1
      }
    }

    preemptible_worker_config {
      num_instances  = var.secondary_num_instances
      preemptibility = var.preemptibility

      disk_config {
        boot_disk_type    = var.secondary_boot_disk_type    #"pd-standard"
        boot_disk_size_gb = var.secondary_boot_disk_size_gb #30
        num_local_ssds    = var.secondary_num_local_ssds    #1
      }
    }

    software_config {
      image_version = var.image_version
      override_properties = var.metastore_service ? merge({
        "hive:hive.metastore.uris"                                        = google_dataproc_metastore_service.metastore_service[0].endpoint_uri
        "hive:hive.metastore.warehouse.dir"                               = google_dataproc_metastore_service.metastore_service[0].artifact_gcs_uri
        "dataproc:dataproc.logging.stackdriver.job.driver.enable"         = true
        "dataproc:dataproc.logging.stackdriver.job.yarn.container.enable" = true
      }, var.software_override_properties) : var.software_override_properties
    }

    dynamic "metastore_config" {
      for_each = google_dataproc_metastore_service.metastore_service
      content {
        dataproc_metastore_service = metastore_config.value["id"]
      }
    }

    gce_cluster_config {
      zone                   = var.multi_zone ? local.cluster_zones[count.index] : ""
      internal_ip_only       = var.internal_ip_only
      tags                   = var.tags
      network                = var.network
      subnetwork             = var.subnetwork
      service_account        = google_service_account.dataproc[0].email
      service_account_scopes = var.service_account_scopes
    }

    dynamic "initialization_action" {
      for_each = var.initialization_action_scripts

      content {
        script      = initialization_action.value #"gs://dataproc-initialization-actions/stackdriver/stackdriver.sh"
        timeout_sec = 500
      }
    }
  }

  lifecycle {
    ignore_changes = [
      labels["goog-dataproc-cluster-name"],
      labels["goog-dataproc-cluster-uuid"],
      labels["goog-dataproc-location"],
      cluster_config[0].preemptible_worker_config,
      cluster_config[0].initialization_action,
      cluster_config[0].gce_cluster_config,
      cluster_config[0].worker_config[0].num_instances
    ]
  }

  depends_on = [
    google_project_service.activate_api,
    module.dataproc_project_iam
  ]

  timeouts {
    create = "60m"
    delete = "2h"
  }
}


/***********************************
  Dataproc Metastore Service
***********************************/
resource "google_dataproc_metastore_service" "metastore_service" {
  count = var.metastore_service ? 1 : 0

  provider   = google-beta
  project    = var.project
  service_id = "${local.name}-dataproc-metastore-${format("%.2s", random_id.name.dec)}"
  location   = var.cluster_region
  port       = var.metastore_port #9080
  tier       = var.metastore_tier #"DEVELOPER"

  labels = var.labels

  maintenance_window {
    hour_of_day = var.metastore_maintenance_window_hour_of_day #2
    day_of_week = var.metastore_maintenance_window_day_of_week #"SUNDAY"
  }

  hive_metastore_config {
    version = var.hive_metastore_version #"2.3.6"
    config_overrides = merge({
      "hive.metastore.warehouse.dir" = "${module.metastore_bucket[0].url}/hive-warehouse"
    }, var.metastore_config_overrides)
  }

  depends_on = [
    google_project_service.activate_api,
    module.dataproc_project_iam
  ]
}