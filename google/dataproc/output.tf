locals {
  cluster_config     = { for cluster_config in google_dataproc_cluster.cluster[*].cluster_config[*] : format("%s", cluster_config[0].gce_cluster_config[0].zone) => cluster_config[0] }
  gce_cluster_config = { for cluster_config in google_dataproc_cluster.cluster[*].cluster_config[*] : format("%s", cluster_config[0].gce_cluster_config[0].zone) => cluster_config[0].gce_cluster_config[0] }

  software_config                  = google_dataproc_cluster.cluster[0].cluster_config[0].software_config[0]
  optional_components_clean_output = coalesce(local.software_config.optional_components, [])
  override_properties_clean_output = coalesce(local.software_config.override_properties, {})
  software_config_clean_output     = merge(local.software_config, { optional_components = local.optional_components_clean_output }, { override_properties = local.override_properties_clean_output })

  cluster_zone = [for cluster_config in google_dataproc_cluster.cluster[*].cluster_config[*] : cluster_config[0].gce_cluster_config[0].zone]
  cluster_names = { for cluster in google_dataproc_cluster.cluster[*] : format("%s", cluster.cluster_config[0].gce_cluster_config[0].zone) => cluster.name }

  metadata_clean_output           = coalesce({ for zone, gce_cluster_config in local.gce_cluster_config : zone => gce_cluster_config["metadata"] }, {})
  gce_cluster_config_exclude      = { for zone, gce_cluster_config in local.gce_cluster_config : zone => { for k, v in gce_cluster_config : k => v if(k != "service_account_scopes") } }
  gce_cluster_config_clean_output = merge(local.gce_cluster_config_exclude, { metadata = local.metadata_clean_output })

  cluster_config_clean_output = merge(local.cluster_config, { gce_cluster_config = [local.gce_cluster_config_clean_output] }, { software_config = [local.software_config_clean_output] })
  cluster_clean_output        = merge({ for cluster in google_dataproc_cluster.cluster[*] : format("%s", cluster.cluster_config[0].gce_cluster_config[0].zone) => cluster }, { cluster_config = [local.cluster_config_clean_output] })
}

output "cluster" {
  value = local.cluster_clean_output
}

output "cluster_config" {
  value = local.cluster_config_clean_output
}

output "cluster_zone" {
  value = local.cluster_zones
}

output "cluster_names" {
  value = local.cluster_names
}

output "metastore_endpoint_uri" {
  value = try(coalesce(google_dataproc_metastore_service.metastore_service[*].endpoint_uri...), null)
}

output "metastore_artifact_gcs_uri" {
  value = try(coalesce(google_dataproc_metastore_service.metastore_service[*].artifact_gcs_uri...), null)
}

output "metastore_service_id" {
  value = try(coalesce(google_dataproc_metastore_service.metastore_service[*].id...), null)
}