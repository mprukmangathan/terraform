<!-- BEGIN_TF_DOCS -->
# Google Dataproc

A module for creating a Dataproc Cluster and related resources

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | n/a |
| <a name="provider_google-beta"></a> [google-beta](#provider\_google-beta) | n/a |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_metastore_bucket"></a> [metastore\_bucket](#module\_metastore\_bucket) | ./modules/storage | n/a |
| <a name="module_staging_bucket"></a> [staging\_bucket](#module\_staging\_bucket) | ./modules/storage | n/a |
| <a name="module_temp_bucket"></a> [temp\_bucket](#module\_temp\_bucket) | ./modules/storage | n/a |

## Resources

| Name | Type |
|------|------|
| [google-beta_google_dataproc_metastore_service.metastore_service](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_dataproc_metastore_service) | resource |
| [google-beta_google_project_service_identity.metastore](https://registry.terraform.io/providers/hashicorp/google-beta/latest/docs/resources/google_project_service_identity) | resource |
| [google_dataproc_cluster.cluster](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dataproc_cluster) | resource |
| [google_project_iam_member.dataproc_sa_binding](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.dataproc_sa_metastore_binding](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.metastore_sa_binding](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_service.activate_api](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_service) | resource |
| [google_service_account.dataproc](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [random_id.name](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_admins"></a> [admins](#input\_admins) | n/a | `list` | `[]` | no |
| <a name="input_application_name"></a> [application\_name](#input\_application\_name) | n/a | `string` | `"demo"` | no |
| <a name="input_autoscaling_policy_id"></a> [autoscaling\_policy\_id](#input\_autoscaling\_policy\_id) | n/a | `string` | `""` | no |
| <a name="input_cluster_region"></a> [cluster\_region](#input\_cluster\_region) | n/a | `string` | `"us-central1"` | no |
| <a name="input_cluster_zones"></a> [cluster\_zones](#input\_cluster\_zones) | n/a | `list(string)` | <pre>[<br>  "us-central1-a",<br>  "us-central1-b"<br>]</pre> | no |
| <a name="input_dataproc_service_account"></a> [dataproc\_service\_account](#input\_dataproc\_service\_account) | n/a | `string` | `""` | no |
| <a name="input_developers"></a> [developers](#input\_developers) | n/a | `list` | `[]` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | n/a | `string` | `"dev"` | no |
| <a name="input_hive_metastore_config"></a> [hive\_metastore\_config](#input\_hive\_metastore\_config) | n/a | `string` | `"2.3.6"` | no |
| <a name="input_hive_metastore_version"></a> [hive\_metastore\_version](#input\_hive\_metastore\_version) | n/a | `string` | `"2.3.6"` | no |
| <a name="input_image_version"></a> [image\_version](#input\_image\_version) | n/a | `string` | `"1.5.53-debian10"` | no |
| <a name="input_initialization_action_scripts"></a> [initialization\_action\_scripts](#input\_initialization\_action\_scripts) | n/a | `map(string)` | `{}` | no |
| <a name="input_internal_ip_only"></a> [internal\_ip\_only](#input\_internal\_ip\_only) | n/a | `bool` | `true` | no |
| <a name="input_labels"></a> [labels](#input\_labels) | n/a | `map(any)` | `{}` | no |
| <a name="input_master_boot_disk_size_gb"></a> [master\_boot\_disk\_size\_gb](#input\_master\_boot\_disk\_size\_gb) | n/a | `number` | `30` | no |
| <a name="input_master_boot_disk_type"></a> [master\_boot\_disk\_type](#input\_master\_boot\_disk\_type) | n/a | `string` | `"pd-ssd"` | no |
| <a name="input_master_machine_type"></a> [master\_machine\_type](#input\_master\_machine\_type) | n/a | `string` | `"n1-standard-2"` | no |
| <a name="input_master_min_cpu_platform"></a> [master\_min\_cpu\_platform](#input\_master\_min\_cpu\_platform) | n/a | `string` | `""` | no |
| <a name="input_master_num_instances"></a> [master\_num\_instances](#input\_master\_num\_instances) | n/a | `number` | `1` | no |
| <a name="input_master_num_local_ssds"></a> [master\_num\_local\_ssds](#input\_master\_num\_local\_ssds) | n/a | `number` | `1` | no |
| <a name="input_metastore_allow_destroy"></a> [metastore\_allow\_destroy](#input\_metastore\_allow\_destroy) | n/a | `bool` | `false` | no |
| <a name="input_metastore_config_overrides"></a> [metastore\_config\_overrides](#input\_metastore\_config\_overrides) | n/a | `map(string)` | `{}` | no |
| <a name="input_metastore_maintenance_window_day_of_week"></a> [metastore\_maintenance\_window\_day\_of\_week](#input\_metastore\_maintenance\_window\_day\_of\_week) | n/a | `string` | `"SUNDAY"` | no |
| <a name="input_metastore_maintenance_window_hour_of_day"></a> [metastore\_maintenance\_window\_hour\_of\_day](#input\_metastore\_maintenance\_window\_hour\_of\_day) | n/a | `number` | `2` | no |
| <a name="input_metastore_port"></a> [metastore\_port](#input\_metastore\_port) | n/a | `number` | `9080` | no |
| <a name="input_metastore_service"></a> [metastore\_service](#input\_metastore\_service) | n/a | `bool` | `false` | no |
| <a name="input_metastore_service_account"></a> [metastore\_service\_account](#input\_metastore\_service\_account) | n/a | `string` | `""` | no |
| <a name="input_metastore_tier"></a> [metastore\_tier](#input\_metastore\_tier) | n/a | `string` | `"DEVELOPER"` | no |
| <a name="input_multi_zone"></a> [multi\_zone](#input\_multi\_zone) | n/a | `bool` | `false` | no |
| <a name="input_network"></a> [network](#input\_network) | n/a | `string` | `""` | no |
| <a name="input_preemptibility"></a> [preemptibility](#input\_preemptibility) | n/a | `string` | `"PREEMPTIBLE"` | no |
| <a name="input_primary_boot_disk_size_gb"></a> [primary\_boot\_disk\_size\_gb](#input\_primary\_boot\_disk\_size\_gb) | n/a | `number` | `20` | no |
| <a name="input_primary_boot_disk_type"></a> [primary\_boot\_disk\_type](#input\_primary\_boot\_disk\_type) | n/a | `string` | `"pd-standard"` | no |
| <a name="input_primary_machine_type"></a> [primary\_machine\_type](#input\_primary\_machine\_type) | n/a | `string` | `"n1-standard-2"` | no |
| <a name="input_primary_min_cpu_platform"></a> [primary\_min\_cpu\_platform](#input\_primary\_min\_cpu\_platform) | n/a | `string` | `""` | no |
| <a name="input_primary_num_instances"></a> [primary\_num\_instances](#input\_primary\_num\_instances) | n/a | `number` | `2` | no |
| <a name="input_primary_num_local_ssds"></a> [primary\_num\_local\_ssds](#input\_primary\_num\_local\_ssds) | n/a | `number` | `1` | no |
| <a name="input_project"></a> [project](#input\_project) | n/a | `string` | `""` | no |
| <a name="input_secondary_boot_disk_size_gb"></a> [secondary\_boot\_disk\_size\_gb](#input\_secondary\_boot\_disk\_size\_gb) | n/a | `number` | `20` | no |
| <a name="input_secondary_boot_disk_type"></a> [secondary\_boot\_disk\_type](#input\_secondary\_boot\_disk\_type) | n/a | `string` | `"pd-standard"` | no |
| <a name="input_secondary_num_instances"></a> [secondary\_num\_instances](#input\_secondary\_num\_instances) | n/a | `number` | `0` | no |
| <a name="input_secondary_num_local_ssds"></a> [secondary\_num\_local\_ssds](#input\_secondary\_num\_local\_ssds) | n/a | `number` | `1` | no |
| <a name="input_service_account"></a> [service\_account](#input\_service\_account) | n/a | `string` | `null` | no |
| <a name="input_service_account_scopes"></a> [service\_account\_scopes](#input\_service\_account\_scopes) | n/a | `list(string)` | <pre>[<br>  "cloud-platform"<br>]</pre> | no |
| <a name="input_software_override_properties"></a> [software\_override\_properties](#input\_software\_override\_properties) | n/a | `map(string)` | `{}` | no |
| <a name="input_subnetwork"></a> [subnetwork](#input\_subnetwork) | n/a | `string` | `""` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | n/a | `list(string)` | <pre>[<br>  "dataproc"<br>]</pre> | no |
| <a name="input_viewers"></a> [viewers](#input\_viewers) | n/a | `list` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_clean_output"></a> [cluster\_clean\_output](#output\_cluster\_clean\_output) | n/a |
| <a name="output_cluster_config"></a> [cluster\_config](#output\_cluster\_config) | n/a |
| <a name="output_gce_cluster_config"></a> [gce\_cluster\_config](#output\_gce\_cluster\_config) | n/a |
| <a name="output_metastore_artifact_gcs_uri"></a> [metastore\_artifact\_gcs\_uri](#output\_metastore\_artifact\_gcs\_uri) | n/a |
| <a name="output_metastore_endpoint_uri"></a> [metastore\_endpoint\_uri](#output\_metastore\_endpoint\_uri) | n/a |
| <a name="output_metastore_service_id"></a> [metastore\_service\_id](#output\_metastore\_service\_id) | n/a |
| <a name="output_software_config"></a> [software\_config](#output\_software\_config) | n/a |
<!-- END_TF_DOCS -->