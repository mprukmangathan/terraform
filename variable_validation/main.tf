provider "google" {
  credentials = file("../google/model-hexagon-351804-6b3b6f158f1e.json")
  region      = "us-central1"
  project     = "model-hexagon-351804"
}

locals {
  is_true    = false
  valid_zone = [for zone in var.cluster_zones : substr(zone, 0, length(var.cluster_region)) == var.cluster_region]
  valid      = [for zone in var.cluster_zones : contains(data.google_compute_zones.available.names, zone) == true ? local.is_true == true : local.is_true == false]
}


data "google_compute_zones" "available" {
  region = var.cluster_region
}


variable "cluster_region" {
  type    = string
  default = "us-central1"
}

variable "cluster_zones" {
  type    = list(string)
  default = ["us-central1-a", "us-central1-b"] #["us-central1-a", "us-central1-b"]

  validation {
    condition     = can(length(var.cluster_zones) < 2)
    error_message = "The number of zones must be grater than 1."
  }
}

output "validation" {
  value = local.is_true
}